# Robotics Assessment CS3027 2018

Submission by Julian Lorenz.

# How to build

*Note: This submission was written and tested on ROS Melodic.*

This submission requires:

+ The PCL point cloud library
+ Python numpy

`cd` into the workspace folder and run these commands:

``` bash
# build source files
catkin_make

source devel/setup.bash

# preprocess the map (only needed once)
./pre.sh
# run these commands in another terminal
rosrun path_planning build_weights
rosrun localisation build_likelihood

# stop all ros commands again
```

# How to run

## Base

`cd` into the workspace folder and run these commands:

``` bash
source devel/setup.bash
# this will launch roscore, stage, RViz and some background nodes like A*, sensor noise, ...
./run.sh
```

There is file called `default.rviz` which contains the settings for RViz to show all the relevant markers.

## The nodes/tasks

### Direct

This node enables you to select goal points using RViz where the robot should move to. Useful for testing task 4.

``` bash
# make sure you don't have any other node form the driving package running!
rosrun driving direct.py
```

### Task 1

Using the launch file, all the required nodes should be started automatically. The following topics are pubished:

+ vis_path_points: Goal positions (green if visited, red if not)
+ robot_block: A blue cube that represents the physical size of the robot
+ robot_track: The true track of the robot

You can use TF to have a look at the created real_robot_pose frame.

### Path follower (Tasks 2 and 3)

Requests the shortest path connecting the goals given in the `assessment.launch` file and follows it.

``` bash
# use the -v option to only visualise the path instead of following it
rosrun driving simple_follow.py [-v]
```

The node caches paths. Performance is increased when a path can be reused from the cache. Cached paths are located in the path_cache folder.

### Particle filter (Task 4)

``` bash
# omit the parameters to use the default parameters
rosrun localisation particle_filter [-p PARTICLES_COUNT] [-u UNIFORM_RATIO] [-m MOVE_ERROR] [-t TURN_ERROR]
```

You might want to run the direct node to test the particle filter (see for section "Direct" above).

To visualise the particle filter, show the particles_array topic in RViz. The expected pose is available through the expected_pose topic. Alternatively you can use robot_track_expected to have a look at the expected track of the robot.

If you wish to do localisation without sensor noise, change this line and recompile using `catkin_make`:

``` cpp
// line 431 in particle_filter.cpp
subScan = n.subscribe("/noisy_base_scan", 1000, &ParticleFilter::handleScan, this);

// change to:

subScan = n.subscribe("/base_scan", 1000, &ParticleFilter::handleScan, this);
```

### Task 5

You need to run two nodes, to get this task done:

``` bash
# takes and stores snapshots from the camera and visualises them in RViz
roslaunch image_processing cloud.launch

# Don't forget to put the robot in front of a red object first!
# this node will then follow the red object and will take snapshots from time to time
rosrun driving paparazzi.py
```

The robot will now circle around the object in front of it and will take snapshots from time to time. The resulting point cloud will be visible in RViz as the `Acc Point Cloud` marker (vis_accu topic).

# License

[CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)