#!/usr/bin/env python

# Calculates the length of a path from a path cache file
# Usage: ./length.py FILENAME FILENAME ...

import sys
from math import sqrt

if len(sys.argv) < 2:
    print "Usage:", sys.argv[0], "FILENAME FILENAME ..."
    exit()

length = 0
for filename in sys.argv[1:]:
    with open(filename) as f:
        last_x = None
        last_y = None
        for line in f.readlines():
            split = line.strip().split(' ')
            x = float(split[0])
            y = float(split[1])

            if not last_x is None:
                length += sqrt((x - last_x) ** 2 + (y - last_y) ** 2)

            last_x = x
            last_y = y

print length
