# Things to ask

## 23/10/18

**Issues**

1. Best approach for localisation. Right now using:
    + Likelihood field
    + Partile filter/Monte Carlo
2. How to use `/odom`? Completely wrong. What is the covariance matrix?
3. Particle filter: point clouds jump
4. Likelihood field: only correct at specific point, not surrounding area (correct?)
5. Map precomputed correctly?
6. Particles: angles?

**Comments**

+ try less rays
+ normalise the weights of the particles
+ check if C++ distribution function works as expected
+ check if copy by value or ref when resampling
+ depth_image_proc can be used (including the /depth topic)

## 06/11/18

**Issues**

1. On which machines is the code tested?
2. Generate uniformly distributed particles every step? (And ignoring the weights)
3. Check: Likelihood field computed correctly
4. Path finding: Precompute paths allowed? (travelling salesman problem)
5. Driving: In the tasks it says:
    > Avoiding obstacles not found on the map while still getting to destination
    
    Is just following an obstacle enough?
6. Guessed location of particle filter: Highest likelihood, median, mean, mixture?
7. Image processing is ok as it is?

**Comments**

1. Doesn't matter
2. Yes! About 1%, 5% maybe even 10%. Do this after the resampling step and ignore it for the guessed position
3. Skipped
4. Precomputing is fine
5. My modified Bug2 should work
6. try weighted mean of all points
7. Yes

## 13/11/18

1. What does "enable errors while driving" mean exactly? Errors for the sensor? Robobt turns slightly?
2. Task 1: Display robot's position using odometry information. What odometry? Task 4?
3. What to do with particles that collide with obstacles and are to far away from the map's borders?

**Comments**

1. In the map.world file, if localization is set to "odom", then errors are used. If localization is set to "gps", errors are disabled. So it should be fine.
2. It's enough to display the TF transform using RViz.
3. Resample them particles that collide.