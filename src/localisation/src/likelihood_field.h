#pragma once

#include <nav_msgs/OccupancyGrid.h>
#include <boost/math/distributions/normal.hpp>

class LikelihoodField
{
  private:
    double *probabilities;
    // make the likelihood field as big as the map
    int xCount = 1000;
    int yCount = 800;
    int xHalfCount, yHalfCount;
    const int extend = 50;
    // cells per meter
    double xyResolution = 1.0 / 0.012;
  public:
    ~LikelihoodField();
    double getLikelihood(double x, double y);
    double getLikelihood(double x, double y, double angle, double distance);
    nav_msgs::OccupancyGrid visualiseField();
    bool loadFromFile();
};