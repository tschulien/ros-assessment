#include <ros/ros.h>
#include <nav_msgs/OccupancyGrid.h>
#include <nav_msgs/GetMap.h>

#include <stdlib.h>
#include <boost/math/distributions/normal.hpp>
#include <fstream>

class ProcessLikelihoods
{
  private:
    nav_msgs::OccupancyGrid mapData;
    double *likelihoodField;
    boost::math::normal obstError;

    const int maxDistance = 50;
    // extended width and height of the field
    int extWidth;
    int extHeight;

    void addNormalDistributionValue(int x, int y, double value)
    {
        // check out of bounds
        if (x < 0 || y < 0 || x >= extWidth || y >= extHeight)
            return;

        likelihoodField[y * extWidth + x] += value;
    }

    void putCircle(int x0, int y0, int radius, double value)
    {
        // https://en.wikipedia.org/wiki/Midpoint_circle_algorithm
        int x = radius - 1;
        int y = 0;
        int dx = 1;
        int dy = 1;
        int err = dx - (radius << 1);

        while (x >= y)
        {
            addNormalDistributionValue(x0 + x, y0 + y, value);
            addNormalDistributionValue(x0 + y, y0 + x, value);
            addNormalDistributionValue(x0 - y, y0 + x, value);
            addNormalDistributionValue(x0 - x, y0 + y, value);
            addNormalDistributionValue(x0 - x, y0 - y, value);
            addNormalDistributionValue(x0 - y, y0 - x, value);
            addNormalDistributionValue(x0 + y, y0 - x, value);
            addNormalDistributionValue(x0 + x, y0 - y, value);

            if (err <= 0)
            {
                y++;
                err += dy;
                dy += 2;
            }
            
            if (err > 0)
            {
                x--;
                dx += 2;
                err += dx - (radius << 1);
            }
        }
    }

    bool isOuter(int shiftedX, int shiftedY)
    {
        int x = shiftedX - maxDistance;
        int y = shiftedY - maxDistance;

        if (x == 0 || x == mapData.info.width - 1 || y == 0 || y == mapData.info.height - 1)
        {
            // boundary
            return true;
        }

        if (mapData.data[y * mapData.info.width + x] < 50)
        {
            // this pixel is not occupied => no outer
            return false;
        }

        int notOccupied = 0;

        for (int row = -1; row <= 1; ++row)
        {
            if ((row + y) < 0 || (row + y) >= mapData.info.height)
                continue;

            for (int col = -1; col <= 1; ++col)
            {
                if (row == 0 && col == 0)
                    continue;
                if ((col + x) < 0 || (col + x) >= mapData.info.width)
                    continue;

                if (mapData.data[(row + y) * mapData.info.width + (col + x)] < 50)
                {
                    // this is not an obstacle
                    // the querried point is an outer point
                    ++notOccupied;
                    // to be an outer point, you need at least two neighbours
                    // that are not occupied
                    if (notOccupied >= 2)
                        return true;
                }
            }
        }
        return false;
    }

    void addNormalDistribution(int x, int y)
    {
        for (int r = maxDistance; r >= 0; --r)
        {
            // convert distance to meters
            double prob = pdf(obstError, r * mapData.info.resolution);
            putCircle(x, y, r, prob);
        }
    }

  public:
    ProcessLikelihoods(nav_msgs::OccupancyGrid& data)
    {
        mapData = data;
    }
    void process()
    {
        const double sd = 0.08;
        const double uniformProbability = 0.1;

        obstError = boost::math::normal(0.0, sd);
        int width = mapData.info.width;
        int height = mapData.info.height;
        extWidth = width + 2 * maxDistance;
        extHeight = height + 2 * maxDistance;

        ROS_INFO("Building likelihood field using SD = %f and uniform prob = %f", sd, uniformProbability);

        ros::Time startTime = ros::Time::now();

        likelihoodField = (double *)malloc(sizeof(double) * extWidth * extHeight);
        // initialise with uniform distribution probability everywhere
        for (int i = 0; i < extWidth * extHeight; ++i)
        {
            likelihoodField[i] = uniformProbability;
        }

        // add normal distributions for each outer pixel
        for (int y = maxDistance; y < height + maxDistance; ++y)
        {
            for (int x = maxDistance; x < width + maxDistance; ++x)
            {
                if (isOuter(x, y))
                {
                    addNormalDistribution(x, y);
                }
            }
            if (y % 50 == 0)
            {
                ROS_INFO("Likelihood field: row %d of %d", y - maxDistance, height);
            }
        }

        ROS_INFO("Write to file");

        std::ofstream likelihoodFile("likelihood.csv");
        // writing to the file
        if (likelihoodFile.is_open())
        {
            for (int y = 0; y < extHeight; ++y)
            {
                likelihoodFile << likelihoodField[y * extWidth];
                for (int x = 1; x < extWidth; ++x)
                {
                    likelihoodFile << ',' << likelihoodField[y * extWidth + x];
                }
                likelihoodFile << '\n';
            }
            likelihoodFile.close();
        }
        else
        {
            ROS_ERROR("Could not save likelihood field to CSV");
        }

        free(likelihoodField);
        ros::Time endTime = ros::Time::now();

        ROS_INFO("Done. Took %f minutes", (endTime - startTime).toSec() / 60.0);
    }
};

int main(int argc, char **argv)
{
    ros::init(argc, argv, "build_likelihood");
    ros::NodeHandle n;

    ros::service::waitForService("/static_map");
    ros::ServiceClient mapClient = n.serviceClient<nav_msgs::GetMap>("/static_map");
    nav_msgs::GetMap srv;
    if (!mapClient.call(srv))
    {
        ROS_ERROR("Failed to call service static_map");
        return 1;
    }

    ProcessLikelihoods(srv.response.map).process();
    return 0;
}
