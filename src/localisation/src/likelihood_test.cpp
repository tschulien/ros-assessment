#include <ros/ros.h>
#include <std_msgs/ColorRGBA.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/PointStamped.h>
#include <nav_msgs/OccupancyGrid.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/LaserScan.h>
#include <visualization_msgs/Marker.h>
#include <math.h>

#include "likelihood_field.h"
#include "util.hpp"

class LikelihoodTest
{
  private:
    ros::Publisher pubVis, pubField;
    ros::Subscriber subClicked, subScan, subBPGT;

    LikelihoodField field;
    visualization_msgs::Marker marker;

    bool scanIsSet = false;
    sensor_msgs::LaserScan laserScan;
    bool odomIsSet = false;
    nav_msgs::Odometry odom;

    double visSensorUpdate(double x, double y, const sensor_msgs::LaserScan& sensorData)
    {
        double yaw = getYaw(odom.pose.pose.orientation);
        visualization_msgs::Marker checks;
        checks.header.stamp = ros::Time::now();
        checks.header.frame_id = "/map";
        checks.scale.x = 0.1;
        checks.scale.y = 0.1;
        checks.scale.z = 0.1;

        checks.action = visualization_msgs::Marker::ADD;
        checks.type = visualization_msgs::Marker::SPHERE_LIST;
        checks.id = 5;
        checks.ns = "specific";

        checks.pose.orientation.x = 0.0;
        checks.pose.orientation.y = 0.0;
        checks.pose.orientation.z = 0.0;
        checks.pose.orientation.w = 1.0;

        checks.color.r = 1.0;
        checks.color.g = 0.3;
        checks.color.b = 0.0;
        checks.color.a = 0.75;

        for (int i = 0; i < sensorData.ranges.size(); ++i)
        {
            double range = sensorData.ranges[i];
            double angle = sensorData.angle_min + i * sensorData.angle_increment + yaw;
            double px = cos(angle) * range + x;
            double py = sin(angle) * range + y;
            double l = field.getLikelihood(px, py);
            l = log(l);

            if (sensorData.intensities[i] < 0.1)
            {
                // ignore this ray
            }
            else
            {
                geometry_msgs::Point p;
                p.x = px;
                p.y = py;
                p.z = l * 0.1;
                checks.points.push_back(p);

                ROS_INFO("weight %d: %f", i, l);
            }

        }

        pubVis.publish(checks);

        double pose[3];
        pose[0] = x;
        pose[1] = y;
        pose[2] = yaw;
        return sensorUpdate(pose, sensorData, field);
    }

    void handleScan(const sensor_msgs::LaserScan& scan)
    {
        laserScan = scan;
        scanIsSet = true;
    }

    void handleBPGT(const nav_msgs::Odometry &bpgt)
    {
        odom = bpgt;
        odomIsSet = true;
    }

    void publishField(const sensor_msgs::LaserScan &scan)
    {
        visualization_msgs::Marker points;
        points.action = visualization_msgs::Marker::ADD;
        points.type = visualization_msgs::Marker::SPHERE_LIST;
        points.id = 1;
        points.ns = "grid_test";

        points.header.stamp = ros::Time::now();
        points.header.frame_id  = "/map";

        points.scale.x = 0.05;
        points.scale.y = 0.05;
        points.scale.z = 0.05;

        double min = DBL_MAX;
        double max = DBL_MIN;

        int width = 120 + 1;
        int height = 96 + 1;
        int width_2 = (width - 1) / 2;
        int height_2 = (height - 1) / 2;

        double sensors[height * width];

        double yaw = getYaw(odom.pose.pose.orientation);
        double pose[3];
        pose[2] = yaw;

        // draw a grid with positions
        for (int y = 0; y < height; ++y)
        {
            for (int x = 0; x < width; ++x)
            {
                pose[0] = 0.1 * (x - width_2);
                pose[1] = 0.1 * (y - height_2);
                double sensor = sensorUpdate(pose, scan, field);
                sensors[y * width + x] = sensor;
                if (sensor > max)
                    max = sensor;
                if (sensor < min)
                    min = sensor;
            }
        }

        normalise(sensors, height * width, min, max);

        ROS_INFO("min %f, max %f", min, max);

        for (int y = 0; y < height; ++y)
        {
            for (int x = 0; x < width; ++x)
            {
                double normalised = sensors[y * width + x] / 1000000;
                geometry_msgs::Point p;
                p.x = 0.1 * (x - width_2);
                p.y = 0.1 * (y - height_2);
                p.z = normalised;
                points.points.push_back(p);

                std_msgs::ColorRGBA c;
                c.r = normalised;
                c.g = 1.0 - normalised;
                c.b = 0.0;
                c.a = 1.0;
                points.colors.push_back(c);
            }
        }

        pubVis.publish(points);
    }

    void handleClickedPoint(const geometry_msgs::PointStamped& point)
    {
        if (scanIsSet && odomIsSet)
        {
            // only for the specific point
            double sens = visSensorUpdate(point.point.x, point.point.y, laserScan);
            ROS_INFO("specific weight: %f", sens);
        }
    }

  public:
    void run(int argc, char **argv)
    {
        ros::init(argc, argv, "likelihood_test");
        ros::NodeHandle n;

        pubVis = n.advertise<visualization_msgs::Marker>("particles_vis", 10);
        pubField = n.advertise<nav_msgs::OccupancyGrid>("likelihood_field", 10);

        // try to load the likelihoods from a file
        bool success = field.loadFromFile();
        if (!success)
        {
            ROS_ERROR("Could not load likelihoods file");
            throw std::exception();
        }

        ROS_INFO("Field is ready");

        pubField.publish(field.visualiseField());
        subClicked = n.subscribe("/clicked_point", 100, &LikelihoodTest::handleClickedPoint, this);
        subScan = n.subscribe("/base_scan", 100, &LikelihoodTest::handleScan, this);
        subBPGT = n.subscribe("/base_pose_ground_truth", 10, &LikelihoodTest::handleBPGT, this);

        while (ros::ok() && (!scanIsSet || !odomIsSet))
        {
            ros::spinOnce();
        }

        ros::Rate rate(0.5);

        while (ros::ok())
        {
            rate.sleep();
            ros::spinOnce();
            publishField(laserScan);
        }
    }
};

int main(int argc, char **argv)
{
    LikelihoodTest node;
    node.run(argc, argv);
}
