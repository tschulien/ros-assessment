#pragma once

#include <sensor_msgs/LaserScan.h>
#include <tf/LinearMath/Matrix3x3.h>
#include <tf/LinearMath/Quaternion.h>
#include <math.h>
#include "likelihood_field.h"

inline double getYaw(geometry_msgs::Quaternion& orientation)
{
    double yaw, pitch, roll;
    tf::Quaternion q(
        orientation.x,
        orientation.y,
        orientation.z,
        orientation.w);
    tf::Matrix3x3(q).getEulerYPR(yaw, pitch, roll);
    return yaw;
}

inline double sensorUpdate(double *pose, const sensor_msgs::LaserScan &sensorData, LikelihoodField &likelihoodField)
{
    double x = pose[0];
    double y = pose[1];
    double yaw = pose[2];

    double likelihood = 0;
    for (int i = 0; i < sensorData.ranges.size(); ++i)
    {
        double range = sensorData.ranges[i];
        double angle = sensorData.angle_min + i * sensorData.angle_increment + yaw;

        if (sensorData.intensities[i] < 0.1)
        {
            likelihood += 400;
        }
        else
        {
            likelihood += log(likelihoodField.getLikelihood(x, y, angle, range));
        }
    }

    return likelihood;
}

// normalises the weights
inline void normalise(double *weights, int particleCount, double minWeight, double maxWeight)
{
    const double outMin = 1;
    const double outMax = 100;

    const double ratio = (outMax - outMin) / (maxWeight - minWeight);

    for (int i = 0; i < particleCount; ++i)
    {
        // ad-hoc post processing of the likelihood, works somewhat
        double w = weights[i];
        w = (w - minWeight) * ratio + outMin;
        weights[i] = w * w * w;
    }
}