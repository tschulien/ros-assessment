// usage particle_filter [particles count] [uniform count] [move error]

#include <ros/ros.h>
#include <tf/LinearMath/Quaternion.h>
#include <tf/transform_datatypes.h>

#include <std_msgs/ColorRGBA.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/GetMap.h>
#include <nav_msgs/OccupancyGrid.h>
#include <sensor_msgs/LaserScan.h>
#include <visualization_msgs/Marker.h>

#include <math.h>
#include <mutex>
#include <random>
#include <string>
#include <stdlib.h>

#include "likelihood_field.h"
#include "util.hpp"

double overflowAngle(double angle)
{
    if (angle > M_PI)
    {
        return overflowAngle(angle - 2 * M_PI);
    }
    if (angle <= -M_PI)
    {
        return overflowAngle(angle + 2 * M_PI);
    }
    return angle;
}

class ParticleFilter
{
    std::mutex lock;

    // publishers and subscribers
    ros::Publisher pubMarker, pubField, pubParticles, pubExpected;
    ros::Subscriber subOdom, subScan, subTruth;

    nav_msgs::Odometry odom;
    nav_msgs::Odometry refOdom;
    bool refOdomIsSet = false;

    nav_msgs::Odometry odomTruth;
    nav_msgs::Odometry refOdomTruth;
    bool refOdomTruthIsSet = false;

    sensor_msgs::LaserScan sensorData;
    bool scanIsSet = false;

    int uniformCount = 0;
    int particleCount = 0;

    // contains 3-tuples (x, y, angle)
    // the uniform particles come first, then the resampled ones
    double *particles;
    double *particleWeights;

    int maxWeightParticle;

    // motion delta
    double deltaFwd, deltaRight, deltaYaw;
    double truthYaw;

    nav_msgs::OccupancyGrid staticMap;
    LikelihoodField likelihoodField;

    // random generators
    std::default_random_engine generator;
    std::random_device rd;
    std::mt19937 gen;
    std::normal_distribution<double> fwdError, rightError, yawError;
    std::uniform_real_distribution<double> angleDistr;
    std::uniform_int_distribution<int> xDistr, yDistr;

    void motionDelta()
    {
        geometry_msgs::Point refPos = refOdom.pose.pose.position;
        geometry_msgs::Point pos = odom.pose.pose.position;

        double refYaw = getYaw(refOdom.pose.pose.orientation);
        double yaw = getYaw(odom.pose.pose.orientation);

        // direction vector of reference pose
        double refDirX = cos(refYaw);
        double refDirY = sin(refYaw);

        // vector connecting the two odom poses
        double vX = pos.x - refPos.x;
        double vY = pos.y - refPos.y;

        // project v onto refDir
        double scale = (refDirX * vX + refDirY * vY) / (refDirX * refDirX + refDirY * refDirY);
        double fwdX = scale * refDirX;
        double fwdY = scale * refDirY;

        double rightX = vX - fwdX;
        double rightY = vY - fwdY;

        // set signed deltas
        double rightSign = refDirY * rightX - refDirX * rightY;
        deltaFwd = copysign(hypot(fwdX, fwdY), scale);
        deltaRight = copysign(hypot(rightX, rightY), rightSign);

        // rotation
        deltaYaw = yaw - refYaw;
        truthYaw = getYaw(odomTruth.pose.pose.orientation);
    }

    void motionUpdate(double *pose)
    {
        double fwd = deltaFwd + fwdError(generator);
        double right = deltaRight + rightError(generator);
        double c = cos(pose[2]);
        double s = sin(pose[2]);
        pose[0] += fwd * c - right * s;
        pose[1] += fwd * s + right * c;
        // pose[2] = truthYaw;
        pose[2] = overflowAngle(pose[2] + deltaYaw + yawError(generator));
    }

    void generateUniformParticles(int count)
    {
        double res = staticMap.info.resolution;
        int halfWidth = staticMap.info.width / 2;
        int halfHeight = staticMap.info.height / 2;

        for (int i = 0; i < count; ++i)
        {
            int x = xDistr(generator);
            int y = yDistr(generator);
            int index = y * staticMap.info.width + x;
            if (staticMap.data[index] > 0)
            {
                // this is an occupied cell, try again
                --i;
                continue;
            }
            // create new particle
            particles[3 * i] = (x - halfWidth) * res;
            particles[3 * i + 1] = (y - halfHeight) * res;
            particles[3 * i + 2] = angleDistr(generator);
        }
    }

    void resampleObstalces(std::discrete_distribution<int> &distr)
    {
        // resample particles that are in an obstacle
        const int halfWidth = staticMap.info.width / 2;
        const int halfHeight = staticMap.info.height / 2;
        for (int i = 0; i < particleCount; ++i)
        {
            int x = (int)(particles[3 * i] / staticMap.info.resolution) + halfWidth;
            int y = (int)(particles[3 * i + 1] / staticMap.info.resolution) + halfHeight;
            if (x < 0 || y < 0 || x >= staticMap.info.width || y >= staticMap.info.height || staticMap.data[y * staticMap.info.width + x] > 50)
            {
                int index = distr(gen);
                particles[3 * i] = particles[3 * index];
                particles[3 * i + 1] = particles[3 * index + 1];
                particles[3 * i + 2] = particles[3 * index + 2];

                particleWeights[i] = particleWeights[index];
            }
        }
    }

    void monteCarloStep()
    {
        lock.lock();

        motionDelta();
        refOdom = odom;
        refOdomTruth = odomTruth;

        std::vector<double> weights;
        double maxWeight = DBL_MIN;
        double minWeight = DBL_MAX;

        for (int i = 0; i < particleCount; ++i)
        {
            double *pose = &(particles[3 * i]);
            motionUpdate(pose);
            double newWeight = sensorUpdate(pose, sensorData, likelihoodField);
            particleWeights[i] = newWeight;

            if (newWeight > maxWeight)
            {
                maxWeight = newWeight;
                maxWeightParticle = i;
            }
            if (newWeight < minWeight)
            {
                minWeight = newWeight;
            }
        }
        lock.unlock();

        normalise(particleWeights, particleCount, minWeight, maxWeight);

        // align weights with minimum weight
        for (int i = 0; i < particleCount; ++i)
        {
            weights.push_back(particleWeights[i]);
        }

        std::discrete_distribution<int> distr(weights.begin(), weights.end());

        resampleObstalces(distr);

        // store the old particles for reference
        double old[particleCount * 3];
        for (int i = 0; i < particleCount * 3; ++i)
        {
            old[i] = particles[i];
        }

        // create new particles based on the weights
        for (int i = uniformCount; i < particleCount; ++i)
        {
            int index = distr(gen);
            particles[3 * i] = old[3 * index];
            particles[3 * i + 1] = old[3 * index + 1];
            particles[3 * i + 2] = old[3 * index + 2];

            particleWeights[i] = weights[index];
        }

        // overwrite some of the particles randomly
        generateUniformParticles(uniformCount);

        drawParticles();
    }

    void getExpected(double &x, double &y, double &rotation)
    {
        x = 0;
        y = 0;
        rotation = 0;
        // only take the resampled particles into account
        for (int i = uniformCount; i < particleCount; ++i)
        {
            x += particles[3 * i];
            y += particles[3 * i + 1];
            rotation += particles[3 * i + 2];
        }

        x /= (particleCount - uniformCount);
        y /= (particleCount - uniformCount);
        rotation /= (particleCount - uniformCount);
    }

    void drawParticles()
    {
        geometry_msgs::PoseArray poses;
        poses.header.stamp = ros::Time::now();
        poses.header.frame_id = "/map";

        tf::Quaternion tmpQuat;

        // only draw the resampled particles
        for (int i = uniformCount; i < particleCount; ++i)
        {
            geometry_msgs::Pose po;
            po.position.x = particles[3 * i];
            po.position.y = particles[3 * i + 1];
            po.position.z = 0.0;
            tmpQuat.setRPY(0, 0, particles[3 * i + 2]);
            quaternionTFToMsg(tmpQuat, po.orientation);
            poses.poses.push_back(po);
        }

        pubParticles.publish(poses);

        // draw guessed location
        double expectedX, expectedY, expectedRot;
        getExpected(expectedX, expectedY, expectedRot);
        geometry_msgs::PoseStamped guessed;
        guessed.header.stamp = ros::Time::now();
        guessed.header.frame_id = "/map";

        tf::Quaternion q;
        q.setRPY(0, 0, expectedRot);
        quaternionTFToMsg(q, guessed.pose.orientation);

        guessed.pose.position.x = expectedX;
        guessed.pose.position.y = expectedY;
        guessed.pose.position.z = 0;
        pubExpected.publish(guessed);
    }

    void handleOdom(const nav_msgs::Odometry &newOdom)
    {
        lock.lock();
        odom = newOdom;
        if (!refOdomIsSet)
        {
            refOdom = newOdom;
            refOdomIsSet = true;
        }
        lock.unlock();
    }

    void handleTruth(const nav_msgs::Odometry& bpgt)
    {
        lock.lock();
        odomTruth = bpgt;
        if (!refOdomTruthIsSet)
        {
            refOdomTruth = bpgt;
            refOdomTruthIsSet = true;
        }
        lock.unlock();
    }

    void handleScan(const sensor_msgs::LaserScan &scan)
    {
        lock.lock();
        sensorData = scan;
        scanIsSet = true;
        lock.unlock();
    }

  public:

    ~ParticleFilter()
    {
        free(particles);
        free(particleWeights);
    }

    int run(int argc, char **argv)
    {
        ros::init(argc, argv, "particle_filter");
        ros::NodeHandle n;

        particleCount = 20000;
        double uniformRatio = 0.005;
        double moveError = 0.06;
        double turnError = 0.2;

        if (argc >= 2 && std::strcmp(argv[1], "--help") == 0)
        {
            ROS_INFO("Usage: particle_filter [-p PARTICLES_COUNT] [-u UNIFORM_RATIO] [-m MOVE_ERROR] [-t TURN_ERROR]");
            return 0;
        }

        for (int i = 2; i < argc; i += 2)
        {
            if (std::strcmp(argv[i - 1], "-p") == 0)
            {
                particleCount = boost::lexical_cast<int>(argv[i]);
            }
            else if (std::strcmp(argv[i - 1], "-u") == 0)
            {
                uniformRatio = boost::lexical_cast<double>(argv[i]);
                if (uniformRatio < 0 || uniformRatio > 1)
                {
                    ROS_ERROR("%f is an invalid ratio. Provide a number between 0 and 1.", uniformRatio);
                    return 1;
                }
            }
            else if (std::strcmp(argv[i - 1], "-m") == 0)
            {
                moveError = boost::lexical_cast<double>(argv[i]);
            }
            else if (std::strcmp(argv[i - 1], "-t") == 0)
            {
                turnError = boost::lexical_cast<double>(argv[i]);
            }
            else
            {
                ROS_ERROR("Unknown parameter %d", i - 1);
                return 1;
            }
        }

        uniformCount = (int)(particleCount * uniformRatio);

        ROS_INFO("Using %d particles and %d uniform", particleCount, uniformCount);
        ROS_INFO("Movement error is SD=%f", moveError);
        ROS_INFO("Turn error is SD=%f", turnError);

        particles = (double *)malloc(sizeof(double) * particleCount * 3);
        particleWeights = (double *)malloc(sizeof(double) * particleCount);

        pubMarker = n.advertise<visualization_msgs::Marker>("particles_vis", 10);
        pubParticles = n.advertise<geometry_msgs::PoseArray>("particles_array", 10);
        pubField = n.advertise<nav_msgs::OccupancyGrid>("likelihood_field", 10);
        pubExpected = n.advertise<geometry_msgs::PoseStamped>("expected_pose", 10);

        // request the map
        ros::service::waitForService("/static_map");
        ros::ServiceClient mapClient = n.serviceClient<nav_msgs::GetMap>("/static_map");
        nav_msgs::GetMap srv;
        if (!mapClient.call(srv))
        {
            ROS_ERROR("Failed to call service static_map");
            return 1;
        }
        staticMap = srv.response.map;
        ROS_INFO("Got map");

        bool success = likelihoodField.loadFromFile();
        if (!success)
        {
            ROS_ERROR("Could not load likelihoods file");
            throw std::exception();
        }

        // initialise random
        gen = std::mt19937(rd());
        fwdError = std::normal_distribution<double>(0.0, moveError);
        // right error, does that even make sense?
        rightError = std::normal_distribution<double>(0.0, moveError);
        yawError = std::normal_distribution<double>(0.0, turnError);
        xDistr = std::uniform_int_distribution<int>(0, staticMap.info.width - 1);
        yDistr = std::uniform_int_distribution<int>(0, staticMap.info.height - 1);
        angleDistr = std::uniform_real_distribution<double>(-M_PI, M_PI);

        subOdom = n.subscribe("/odom", 1000, &ParticleFilter::handleOdom, this);
        subTruth = n.subscribe("/base_pose_ground_truth", 1000, &ParticleFilter::handleTruth, this);
        subScan = n.subscribe("/noisy_base_scan", 1000, &ParticleFilter::handleScan, this);

        pubField.publish(likelihoodField.visualiseField());

        ROS_INFO("node is ready");

        // generate particles
        generateUniformParticles(particleCount);

        ROS_INFO("Waiting for sensors");

        // get data from the callbacks
        while (ros::ok() && (!refOdomIsSet || !refOdomTruthIsSet || !scanIsSet))
        {
            ros::spinOnce();
            ros::Duration(0.3).sleep();
        }

        // keep calling the callbacks
        ros::AsyncSpinner spinner(0);
        spinner.start();

        ROS_INFO("Particle filter is ready");

        ros::Rate rate(2);
        while (ros::ok())
        {
            rate.sleep();

            monteCarloStep();
        }
        return 0;
    }
};

int main(int argc, char **argv)
{
    return ParticleFilter().run(argc, argv);
}
