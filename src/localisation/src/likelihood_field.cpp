// TODO: tighter connection between likelihood_field and process_map

#include "likelihood_field.h"

#include <stdlib.h>
#include <ros/ros.h>
#include <nav_msgs/OccupancyGrid.h>
#include <math.h>

#include <boost/math/distributions/normal.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

double LikelihoodField::getLikelihood(double x, double y)
{
    // transform to discrete values
    // add half count, coordinates are 0 in the centre of the map
    int xd = (int)(x * xyResolution) + xHalfCount + extend;
    int yd = (int)(y * xyResolution) + yHalfCount + extend;

    const int extX = xCount + 2 * extend;
    const int extY = yCount + 2 * extend;

    // check bounds
    if (xd < 0 || xd >= extX || yd < 0 || yd >= extY)
    {
        // the uniform probability from process_map
        return 0.1;
    }

    return probabilities[yd * extX + xd];
}

double LikelihoodField::getLikelihood(double x, double y, double angle, double distance)
{
    // calculate the hit point
    double px = cos(angle) * distance + x;
    double py = sin(angle) * distance + y;
    return getLikelihood(px, py);
}

bool LikelihoodField::loadFromFile()
{
    // halves are needed quite often => store them
    xHalfCount = xCount / 2;
    yHalfCount = yCount / 2;

    int extX = xCount + 2 * extend;
    int extY = yCount + 2 * extend;

    std::ifstream csvFile("likelihood.csv");
    if (csvFile.is_open())
    {
        probabilities = (double *)malloc(sizeof(double) * extX * extY);
        std::string line;
        std::string cell;
        int x = 0;
        int y = 0;
        while (std::getline(csvFile, line))
        {
            std::stringstream lineStream(line);
            while (std::getline(lineStream, cell, ','))
            {
                probabilities[y * extX + x] = boost::lexical_cast<double>(cell);
                ++x;
            }
            ++y;
            x = 0;
        }
    }
    else
    {
        return false;
    }
    return true;
}

nav_msgs::OccupancyGrid LikelihoodField::visualiseField()
{
    nav_msgs::OccupancyGrid grid;
    grid.header.frame_id = "/map";
    grid.header.stamp = ros::Time::now();

    grid.info.width = xCount;
    grid.info.height = yCount;
    grid.info.resolution = 0.012;

    grid.info.origin.orientation.x = 0.0;
    grid.info.origin.orientation.y = 0.0;
    grid.info.origin.orientation.z = 0.0;
    grid.info.origin.orientation.w = 1.0;

    grid.info.origin.position.x = -6.0;
    grid.info.origin.position.y = -4.8;
    grid.info.origin.position.z =  0.0;

    // find the maximum values
    double maxLike = 0.0;
    for (int i = 0; i < yCount * xCount; ++i)
    {
        if (probabilities[i] > maxLike)
            maxLike = probabilities[i];
    }

    double scale = 100 / maxLike;
    int extX = xCount + 2 * extend;

    for (int r = extend; r < yCount + extend; ++r)
    {
        for (int c = extend; c < xCount + extend; ++c)
        {
            grid.data.push_back((int)(scale * probabilities[r * extX + c]));
        }
    }
    return grid;
}

LikelihoodField::~LikelihoodField()
{
    free(probabilities);
}