#!/usr/bin/env python

import rospy
import tf
import tf2_ros
from std_msgs.msg import ColorRGBA
from nav_msgs.msg import Odometry, Path
from geometry_msgs.msg import Point, TransformStamped, PoseStamped, Quaternion, Vector3
from visualization_msgs.msg import Marker


class PoseMarker:

    sqr_marker_distance = 0.1 ** 2
    max_path_length = 300

    def __init__(self):
        rospy.init_node('mark_poses')

        ## callback variables
        self.last_marker = None
        self.last_marker_exp = None
        self.robot_track = Path()
        self.robot_track.header.frame_id = '/map'
        self.robot_track_exp = Path()
        self.robot_track_exp.header.frame_id = '/map'

        self.listener = tf.TransformListener()
        self.tf_broadcaster = tf2_ros.TransformBroadcaster()

        self.pub_robot = rospy.Publisher('robot_block', Marker, queue_size=2)
        self.pub_path = rospy.Publisher('robot_track', Path, queue_size=5)
        self.pub_path_exp = rospy.Publisher('robot_track_expected', Path, queue_size=5)
        rospy.Subscriber('/base_pose_ground_truth', Odometry, self.handle_truth)
        rospy.Subscriber('/expected_pose', PoseStamped, self.handle_expected)

    def append_track(self, ps, last_marker, path):
        # new points are only added to the track if the robot moved far enough
        if last_marker is None or (last_marker.x - ps.pose.position.x) ** 2 + (last_marker.y - ps.pose.position.y) ** 2 > self.sqr_marker_distance:
            path.poses.append(ps)
            if len(path.poses) > self.max_path_length:
                del path.poses[0]

            return ps.pose.position
        return None

    def handle_expected(self, data):
        # append to expected track
        new_last = self.append_track(data, self.last_marker_exp, self.robot_track_exp)
        if not new_last is None:
            self.last_marker_exp = new_last
            self.pub_path_exp.publish(self.robot_track_exp)

    def handle_truth(self, data):
        # broadcast transform
        t = TransformStamped()
        t.header.stamp = rospy.Time.now()
        t.header.frame_id = 'map'
        t.child_frame_id = 'real_robot_pose'
        t.transform.translation = data.pose.pose.position
        t.transform.rotation = data.pose.pose.orientation
        self.tf_broadcaster.sendTransform(t)

        # append to true track
        ps = PoseStamped()
        ps.header = data.header
        ps.pose = data.pose.pose
        new_last = self.append_track(ps, self.last_marker, self.robot_track)
        if not new_last is None:
            self.last_marker = new_last
            self.pub_path.publish(self.robot_track)

        # show a blue cube, representing the robot in RViz
        self.publish_robot_block()

    def publish_robot_block(self):
        block = Marker()
        block.id = 0
        block.action = Marker.ADD
        block.type = Marker.CUBE
        block.header.stamp = rospy.Time.now()
        block.header.frame_id = '/real_robot_pose'
        block.pose.position = Point(-0.05, 0, 0)
        block.pose.orientation = Quaternion(0, 0, 0, 1)
        block.scale = Vector3(0.1, 0.1, 0.1)
        block.color = ColorRGBA(0, 0, 1, 1)
        self.pub_robot.publish(block)

if __name__ == '__main__':
    try:
        node = PoseMarker()
        print "ready"
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
