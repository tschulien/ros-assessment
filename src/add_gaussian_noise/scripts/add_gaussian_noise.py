#!/usr/bin/env python

# NOTE: refactored the code

import roslib
import math
import rospy
import numpy
from sensor_msgs.msg import LaserScan


class NoisySensor:
    def __init__(self):
        rospy.init_node('noisy_base_scan')
        self.publisher = rospy.Publisher('/noisy_base_scan', LaserScan, queue_size=2)
        self.subscriber = rospy.Subscriber('/base_scan', LaserScan, self.scan_received)
        try:
            self.mean = rospy.get_param('sensor_mean', 0)
            self.sd = rospy.get_param('sensor_sd', 0.1)
        except (KeyError):
            self.sd = 0.1
            self.mean = 0

    def scan_received(self, laser_scan):
        noise = numpy.random.normal(self.mean, self.sd, size=len(laser_scan.ranges))
        laser_scan.ranges = list(laser_scan.ranges + noise)
        self.publisher.publish(laser_scan)


if __name__ == '__main__':
    try:
        ns = NoisySensor()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
