#include <ros/ros.h>
#include <nav_msgs/OccupancyGrid.h>
#include <nav_msgs/GetMap.h>

#include <stdlib.h>
#include <boost/math/distributions/normal.hpp>
#include <fstream>

class ProcessPathMap
{
  private:
    nav_msgs::OccupancyGrid mapData;

    bool mapOccupied(int x, int y)
    {
        if (x < 0 || y < 0 || x >= 1000 || y >= 800)
            return true;
        return mapData.data[y * 1000 + x] > 0;
    }

    bool checkCircle(int x0, int y0, int radius)
    {
        // https://en.wikipedia.org/wiki/Midpoint_circle_algorithm
        int x = radius - 1;
        int y = 0;
        int dx = 1;
        int dy = 1;
        int err = dx - (radius << 1);

        while (x >= y)
        {
            if (mapOccupied(x0 + x, y0 + y)) return true;
            if (mapOccupied(x0 + y, y0 + x)) return true;
            if (mapOccupied(x0 - y, y0 + x)) return true;
            if (mapOccupied(x0 - x, y0 + y)) return true;
            if (mapOccupied(x0 - x, y0 - y)) return true;
            if (mapOccupied(x0 - y, y0 - x)) return true;
            if (mapOccupied(x0 + y, y0 - x)) return true;
            if (mapOccupied(x0 + x, y0 - y)) return true;

            if (err <= 0)
            {
                y++;
                err += dy;
                dy += 2;
            }
            
            if (err > 0)
            {
                x--;
                dx += 2;
                err += dx - (radius << 1);
            }
        }
        return false;
    }

    // Returns the closest distance in pixels to an obstacle or the boundary.
    int nearestObstacle(int x, int y, const int maxDistance = 40)
    {
        if (mapOccupied(x, y))
            return 0;
        for (int d = 1; d < maxDistance; ++d)
        {
            if (checkCircle(x, y, d))
                return d;
        }
        return -1;
    }

    int cellValuePath(int x, int y)
    {
        const int unsafe = 0.15 / mapData.info.resolution;
        const int nogo = 0.09 / mapData.info.resolution;
        int dist = nearestObstacle(x, y, unsafe);
        // no obstacle nearby?
        if (dist < 0)
            return 0;
        if (dist < nogo)
        {
            // unwalkable
            return INT32_MAX;
        }
        return unsafe - dist;
    }

  public:
    ProcessPathMap(nav_msgs::OccupancyGrid& data)
    {
        mapData = data;
    }
    void process()
    {
        int width = mapData.info.width;
        int height = mapData.info.height;

        std::ofstream pathFile("path_map.csv");
        ROS_INFO("Building path map");
        if (pathFile.is_open())
        {
            for (int y = 0; y < height; ++y)
            {
                pathFile << cellValuePath(0, y);
                for (int x = 1; x < width; ++x)
                {
                    pathFile << ',' << cellValuePath(x, y);
                }
                pathFile << '\n';

                if (y % 50 == 0)
                {
                    ROS_INFO("Path map: row %d of %d", y, height);
                }
            }
            pathFile.close();
        }
        else
        {
            ROS_ERROR("Could not save path map to CSV");
        }

        ROS_INFO("Done");
    }
};

int main(int argc, char **argv)
{
    ros::init(argc, argv, "build_weights");
    ros::NodeHandle n;

    ros::service::waitForService("/static_map");
    ros::ServiceClient mapClient = n.serviceClient<nav_msgs::GetMap>("/static_map");
    nav_msgs::GetMap srv;
    if (!mapClient.call(srv))
    {
        ROS_ERROR("Failed to call service static_map");
        return 1;
    }

    ProcessPathMap(srv.response.map).process();
    return 0;
}