#!/usr/bin/env python

from collections import namedtuple
import rospy
from std_msgs.msg import ColorRGBA
from geometry_msgs.msg import Point, Quaternion
from nav_msgs.msg import Odometry
from visualization_msgs.msg import Marker

import numpy as np

Goal = namedtuple('Goal', ['point', 'visited'])

class VisGoals:

    visited_colour = ColorRGBA(0, 1, 0, 1)
    goal_colour = ColorRGBA(1, 0, 0, 1)
    visit_distance = 0.1

    def __init__(self):
        self.goals = []

    def get_marker(self, goal, markerId):
        scale = 0.2
        mr = Marker()
        mr.id = markerId
        mr.type = Marker.SPHERE
        mr.action = Marker.ADD
        mr.header.frame_id = '/map'
        mr.header.stamp = rospy.Time.now()
        mr.ns = 'path_goals'
        mr.pose.orientation = Quaternion(0, 0, 0, 1)
        mr.pose.position = goal.point
        mr.pose.position.z = scale
        mr.scale.x = scale
        mr.scale.y = scale
        mr.scale.z = scale
        mr.color = self.visited_colour if goal.visited else self.goal_colour
        return mr

    def got_visit(self, goal, position):
        # already visited by the robot
        if goal.visited:
            return True

        if np.hypot(goal.point.x - position.x, goal.point.y - position.y) < self.visit_distance:
            return True

        return False

    def handle_bpgt(self, bpgt):
        pos = bpgt.pose.pose.position
        self.goals = [Goal(g.point, self.got_visit(g, pos)) for g in self.goals]

    def run(self):
        rospy.init_node('vis_goals')

        # collect all goals
        goalIndex = 0
        while rospy.has_param('goal{}'.format(goalIndex)):
            result = rospy.get_param('goal{}'.format(goalIndex))
            point = Point(x=result[0], y=result[1], z=0)
            self.goals.append(Goal(point, False))
            goalIndex += 1

        rospy.Subscriber('/base_pose_ground_truth', Odometry, self.handle_bpgt)
        pub_markers = rospy.Publisher('vis_path_points', Marker, queue_size=10)

        rate = rospy.Rate(1)

        was_reached = len(self.goals) * [False]

        while not rospy.is_shutdown():
            for i, goal in enumerate(self.goals):
                marker = self.get_marker(goal, i)
                pub_markers.publish(marker)

            rate.sleep()

if __name__ == '__main__':
    try:
        VisGoals().run()
    except rospy.ROSInterruptException:
        pass
