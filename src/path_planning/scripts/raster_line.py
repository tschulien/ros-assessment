import math

def raster_line(x0, y0, x1, y1):

    rastered = []

    dx = abs(x1 - x0)
    dy = abs(y1 - y0)

    if dx > dy:
        sign = 1 if x1 > x0 else -1

        m = float(y1 - y0) / float(x1 - x0)
        for x in range(dx + 1):
            y = sign * m * x
            x *= sign
            rastered.append((int(x + x0), int(y + y0)))
            rastered.append((int(math.ceil(x + x0)), int(math.ceil(y + y0))))
    else:
        sign = 1 if y1 > y0 else -1

        m = float(x1 - x0) / float(y1 - y0)
        for y in range(dy + 1):
            x = sign * y * m
            y *= sign
            rastered.append((int(x + x0), int(y + y0)))
            rastered.append((int(math.ceil(x + x0)), int(math.ceil(y + y0))))

    return rastered
