#!/usr/bin/env python

# NOTE: this script requires the preprocessed path map
# (in the directory were the preprocessor creates it)

from collections import namedtuple
from Queue import PriorityQueue
import threading
import numpy as np
import rospy
from geometry_msgs.msg import Point
from path_planning.srv import AStar, AStarResponse
from nav_msgs.srv import GetMap
# from raster_line import raster_line
from bresenham import raster_line
from nav_msgs.msg import OccupancyGrid

Node = namedtuple('Node', ['score', 'closed', 'came_from'])

class AStarException(Exception):
    pass

def distance(row1, col1, row2, col2):
    return np.hypot(row2 - row1, col2 - col1)

class AStarNode:

    def __init__(self):
        rospy.init_node('a_star')
        weight_pub = rospy.Publisher('/a_star_weights', OccupancyGrid, queue_size=2)

        # first, get the map
        rospy.wait_for_service('/static_map')
        get_map = rospy.ServiceProxy('/static_map', GetMap)
        map_resp = get_map().map
        self.width = map_resp.info.width
        self.height = map_resp.info.height
        self.resolution = map_resp.info.resolution

        self.weights, vis = self.compute_cell_weights()
        weight_pub.publish(self.vis_weights(vis))

        self.debug_raw_path = []
        self.index_map_pub = rospy.Publisher('/a_star_raw', OccupancyGrid, queue_size=2)

        self.lock = threading.Lock()

        rospy.Service('a_star', AStar, self.handle_request)
        print "service is ready"

    def vis_weights(self, vis):
        weight_vis = OccupancyGrid()
        weight_vis.header.stamp = rospy.Time.now()
        weight_vis.header.frame_id = '/map'
        weight_vis.info.resolution = self.resolution
        weight_vis.info.width = self.width
        weight_vis.info.height = self.height
        weight_vis.info.origin.position.x = -6.0
        weight_vis.info.origin.position.y = -4.8
        weight_vis.data = vis
        return weight_vis

    def index_path_map(self):
        index_map = OccupancyGrid()
        index_map.header.stamp = rospy.Time.now()
        index_map.header.frame_id = '/map'
        index_map.info.resolution = self.resolution
        index_map.info.width = self.width
        index_map.info.height = self.height
        index_map.info.origin.position.x = -6.0
        index_map.info.origin.position.y = -4.8
        index_map.data = (self.width * self.height) * [0]
        for i in self.debug_raw_path:
            index_map.data[i] = 100
        return index_map

    def index_to_row_col(self, index):
        col = index % self.width
        row = index // self.width
        return row, col

    def row_col_to_index(self, row, col):
        return row * self.width + col

    def get_neighbours(self, row, col):
        neighbours = []

        if row > 0:
            neighbours.append(self.row_col_to_index(row - 1, col))
            if col > 0:
                neighbours.append(self.row_col_to_index(row - 1, col - 1))
            if col < (self.width - 1):
                neighbours.append(self.row_col_to_index(row - 1, col + 1))

        if row < (self.height - 1):
            neighbours.append(self.row_col_to_index(row + 1, col))
            if col > 0:
                neighbours.append(self.row_col_to_index(row + 1, col - 1))
            if col < (self.width - 1):
                neighbours.append(self.row_col_to_index(row + 1, col + 1))

        if col > 0:
            neighbours.append(self.row_col_to_index(row, col - 1))
        if col < (self.width - 1):
            neighbours.append(self.row_col_to_index(row, col + 1))

        return neighbours

    def reconstruct(self, node_index, start_index, nodes):
        total_path = []
        current_index = node_index
        while current_index != start_index:
            total_path.append(current_index)
            # came from
            current_index = nodes[current_index].came_from

        # reverse
        return total_path[::-1]

    def col_row_occupied(self, x, y):
        index = self.row_col_to_index(y, x)
        return self.weights[index] > 0

    def walkable(self, start_index, end_index):
        start_row, start_col = self.index_to_row_col(start_index)
        end_row, end_col = self.index_to_row_col(end_index)

        # find all grid cells on the line that connects start and end
        return not raster_line(start_col, start_row, end_col, end_row, self.col_row_occupied)

    def smooth_reconstruct(self, node_index, start_index, nodes):
        total_path = []

        base = node_index
        while base != start_index:
            turn = nodes[base].came_from

            while self.walkable(base, turn) and turn != start_index:
                turn = nodes[turn].came_from

            total_path.append(base)
            base = turn

        total_path.append(start_index)

        # reverse
        return total_path[::-1]


    # http://docs.ros.org/api/nav_msgs/html/msg/OccupancyGrid.html
    def a_star(self, start_coord, heuristics_function, reached_function):
        # heuristics_function: a function taking two arguments (row and col)
        # and returning the respondent heuristic
        # reached_function: a function taking a node index as an argument
        # and returning if the goal was reached
        start_time = rospy.Time.now()

        start_index = self.row_col_to_index(start_coord[0], start_coord[1])

        queue = PriorityQueue()
        # weight, closed, came from
        nodes = (self.width * self.height) * [Node(-1, False, -1)]

        n = nodes[start_index]
        nodes[start_index] = Node(0, n.closed, n.came_from)

        queue.put((0, start_index))

        while not queue.empty():
            _, node_index = queue.get_nowait()
            # set closed to true
            n = nodes[node_index]
            nodes[node_index] = Node(n.score, True, n.came_from)

            if reached_function(node_index):
                end_time = rospy.Time.now()
                print "Completed A*, time taken:", (end_time - start_time).to_sec()
                # reconstruct path
                reconstr = self.reconstruct(node_index, start_index, nodes)
                smooth = self.smooth_reconstruct(node_index, start_index, nodes)
                return (reconstr, smooth)

            # add neighbours
            row, col = self.index_to_row_col(node_index)
            neighbours = self.get_neighbours(row, col)
            for neighbour in neighbours:
                # closed?
                if nodes[neighbour].closed:
                    continue

                # distance to neighbor
                n_row, n_col = self.index_to_row_col(neighbour)
                dist = distance(row, col, n_row, n_col)

                # node score + distance + weight
                neighbour_score = nodes[node_index].score + dist + self.weights[neighbour]

                # straight line distance to goal
                heuristic = heuristics_function(n_row, n_col)

                # add to priority queue
                tentative = neighbour_score + heuristic
                if nodes[neighbour].score == -1:
                    queue.put((tentative, neighbour))
                    n = nodes[neighbour]
                    nodes[neighbour] = Node(neighbour_score, n.closed, node_index)

                elif neighbour_score < nodes[neighbour].score:
                    n = nodes[neighbour]
                    nodes[neighbour] = Node(neighbour_score, n.closed, node_index)

        # found no path
        raise AStarException()

    def point_to_row_col(self, point):
        col = int(point.x / self.resolution + 0.5 * self.width)
        row = int(point.y / self.resolution + 0.5 * self.height)
        return row, col

    def index_to_point(self, index):
        row, col = self.index_to_row_col(index)
        return Point((col - 0.5 * self.width) * self.resolution, (row - 0.5 * self.height) * self.resolution, 0)

    def handle_request(self, request):
        if self.lock.locked():
            return AStarResponse(success=2)

        print "Got path request"
        start_coord = self.point_to_row_col(request.start)

        self.lock.acquire()

        goal_indices = {}
        goal_coords = []
        for goal in request.goals:
            row, col = self.point_to_row_col(goal)
            goal_coords.append((row, col))
            goal_indices[self.row_col_to_index(row, col)] = goal

        # define heuristic and reached functions
        if len(request.goals) == 1:
            print "{:.2} {:.2} -> {:.2} {:.2}".format(
                request.start.x, request.start.y,
                request.goals[0].x, request.goals[0].y
            )
            goal_row, goal_col = goal_coords[0]
            goal_index = goal_indices.keys()[0]

            def heuristic(row, col):
                return distance(row, col, goal_row, goal_col)

            def reach(node_index):
                return node_index == goal_index
        else:
            print "identified as multi goal path"
            def heuristic(row, col):
                h = float('inf')
                for goal_row, goal_col in goal_coords:
                    goal_dist = distance(row, col, goal_row, goal_col)
                    if goal_dist < h:
                        h = goal_dist
                return h

            def reach(node_index):
                return node_index in goal_indices

        try:
            index_path, smooth_index_path = self.a_star(start_coord, heuristic, reach)
            if not index_path:
                raise AStarException("Got empty path")
            reached_index = index_path[-1]
            reached = goal_indices.pop(reached_index)

            smoothed = [self.index_to_point(p) for p in smooth_index_path]
            # make the actual requested points definitely part of the path
            smoothed.insert(0, request.start)
            smoothed.append(reached)

        except AStarException:
            # could not find a path
            self.lock.release()
            return AStarResponse(success=False, indexPath=[])

        self.lock.release()

        self.debug_raw_path.extend(index_path)
        self.index_map_pub.publish(self.index_path_map())

        return AStarResponse(success=True, indexPath=index_path, path=smoothed, reachedGoal=reached)

    def max_dist(self, x, y):
        return max(abs(x), abs(y))

    def highest_neighbour(self, row, col, raw_data, width, height, no_go_zone, close_zone):
        l = close_zone + no_go_zone

        max_weight = 0
        for r in range(-l + 1, l):
            if r + row < 0 or r + row >= height:
                continue
            for c in range(-l + 1, l):
                if r == 0 and c == 0:
                    continue
                if c + col < 0 or c + col >= width:
                    continue

                index = self.row_col_to_index(r + row, c + col)
                if raw_data[index] != 0:
                    dist = self.max_dist(r, c)
                    if dist <= no_go_zone:
                        # reached max weight
                        return float('inf')
                    weight = close_zone - (dist - no_go_zone) + 1
                    if weight > max_weight:
                        max_weight = weight
        return max_weight

    def compute_cell_weights(self):
        # load from file
        print "loading weights"
        weights = (self.width * self.height) * [0]
        vis = (self.width * self.height) * [0]
        # NOTE: this script requires the preprocessed path!
        weights_filename = '../../../path_map.csv'
        with open(weights_filename) as f:
            for y, line in enumerate(f.readlines()):
                for x, value in enumerate(line.split(',')):
                    int_val = int(value)
                    weights[y * self.width + x] = int_val
                    vis[y * self.width + x] = 100 if int_val > 100 else int_val

        return weights, vis

if __name__ == '__main__':
    try:
        node = AStarNode()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
