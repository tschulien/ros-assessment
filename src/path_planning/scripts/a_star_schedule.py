#!/usr/bin/env python

# NOTE: should be launched from the scripts directory since path cache files are created
# tries all the possible routes and returns the shortest one

from collections import namedtuple
# from Queue import Queue
import Queue as qu
from multiprocessing import Pool, Process, Queue
import itertools
import numpy as np
import os

import rospy
from path_planning.srv import AStar, AStarResponse
from nav_msgs.msg import Path
from geometry_msgs.msg import Point

Precomputed = namedtuple('Precomputed', ['path', 'length'])

path_cache_directory = '../../../path_cache'

class AStarSchedule:
    def __init__(self):
        rospy.init_node('salesman')

        self.a_star_count = rospy.get_param('a_star_count', 2)

        # create cache directory if it does not exist
        if not os.path.isdir(path_cache_directory):
            os.mkdir(path_cache_directory)

        print "Waiting for A* services:", self.a_star_count
        for i in range(self.a_star_count):
            rospy.wait_for_service('a_star_{}'.format(i))

        print "Service is ready"
        rospy.Service('salesman', AStar, self.handle_request)

    def path_filename(self, start_pos, end_pos):
        return "{}/path {:.3} {:.3} {:.3} {:.3}.txt".format(
            path_cache_directory,
            start_pos.x, start_pos.y,
            end_pos.x, end_pos.y
        )

    def save_to_file(self, path):
        filename = self.path_filename(path[0], path[-1])
        with open(filename, "w") as f:
            for p in path:
                f.write("{} {}\n".format(p.x, p.y))

    def load_file(self, start_pos, end_pos):
        filename = self.path_filename(start_pos, end_pos)
        if os.path.isfile(filename):
            path = []
            with open(filename) as f:
                for line in f.readlines():
                    split = line.strip().split(' ')
                    path.append(Point(float(split[0]), float(split[1]), 0))

            return path

        return None

    def schedule(self, topic, goal_queue, paths):
        # prepare service
        find_path = rospy.ServiceProxy(topic, AStar)

        try:
            while True:
                # wait until you can take something from the queue
                p1, p2 = goal_queue.get_nowait()

                # try to read from file first
                path = self.load_file(p1, p2)
                if not path:
                    path = self.load_file(p2, p1)
                if not path:
                    resp = find_path(p1, [p2])
                    path = resp.path
                    self.save_to_file(resp.path)

                paths.put_nowait(path)
        except qu.Empty:
            # nothing left in the queue
            pass

    def get_path_length(self, path):
        length = 0
        last_p = path[0]
        for p in path[1:]:
            length += np.hypot(p.x - last_p.x, p.y - last_p.y)
            last_p = p

        return length

    def build_lookup(self, paths):
        # build a lookup table (dict) that contains the path like this:
        # lookup[start][end] = (<path from start to end>, <path length>)
        path_dict = {}
        for p in paths:
            start = (p[0].x, p[0].y)
            end = (p[-1].x, p[-1].y)
            length = self.get_path_length(p)

            if start in path_dict:
                path_dict[start][end] = Precomputed(p, length)
            else:
                path_dict[start] = { end: Precomputed(p, length) }

            if end in path_dict:
                path_dict[end][start] = Precomputed(p[::-1], length)
            else:
                path_dict[end] = { start: Precomputed(p[::-1], length) }

        return path_dict

    def lookup_length(self, points, lookup):
        length = 0
        last_p = points[0]
        for p in points[1:]:
            length += self.do_lookup(last_p, p, lookup).length
            last_p = p

        return length

    def do_lookup(self, start_pos, end_pos, table):
        start = (start_pos.x, start_pos.y)
        end = (end_pos.x, end_pos.y)
        return table[start][end]

    def handle_request(self, request):
        print "Got a request"

        goal_queue = Queue()
        path_queue = Queue()

        processes = [Process(target=self.schedule, args=('a_star_{}'.format(i), goal_queue, path_queue)) for i in range(self.a_star_count)]

        edges = [request.start]
        edges.extend(request.goals)
        for i, p1 in enumerate(edges):
            for p2 in edges[(i + 1):]:
                goal_queue.put_nowait((p1, p2))

        for proc in processes:
            proc.start()

        for proc in processes:
            proc.join()

        paths = []
        while path_queue.qsize() > 0:
            paths.append(path_queue.get_nowait())

        # build a lookup table
        lookup = self.build_lookup(paths)

        # now brute force to find the best route
        best_path = None
        min_length = float('inf')
        for perm in itertools.permutations(request.goals):
            points = [request.start]
            points.extend(perm)
            length = self.lookup_length(points, lookup)

            if length < min_length:
                min_length = length
                best_path = points

        # assemble path
        full_path = []
        last_p = best_path[0]
        for p in best_path[1:]:
            full_path.extend(self.do_lookup(last_p, p, lookup).path)
            last_p = p

        print "done"

        return AStarResponse(success=1, path=full_path)


if __name__ == '__main__':
    try:
        node = AStarSchedule()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
