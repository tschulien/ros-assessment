#!/usr/bin/env python

import threading
import rospy
from std_msgs.msg import ColorRGBA
from geometry_msgs.msg import Point, PointStamped
from visualization_msgs.msg import Marker
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry
import tf
import numpy as np
import vis
import util
from move import Move

class Direct:

    def __init__(self):
        self.lock = threading.Lock()

        rospy.init_node('direct_single')
        rospy.Subscriber('/base_pose_ground_truth', Odometry, self.handle_bpgt)
        rospy.Subscriber('/base_scan', LaserScan, self.handle_scan)

        rospy.Subscriber('/clicked_point', PointStamped, self.handle_clicked_point)
        rospy.Subscriber('move_goal', Point, self.handle_move_goal)

        self.pub_marker = rospy.Publisher('vis_direct_move', Marker, queue_size=4)

        self.mover = Move()

        self.target = None
        self.target_marker = None
        self.position = None
        self.yaw = None
        self.sensor_data = None

    def handle_scan(self, sensor_data):
        self.lock.acquire()
        self.sensor_data = sensor_data
        self.lock.release()

    def handle_move_goal(self, point):
        self.lock.acquire()
        self.target = point
        self.target_marker = vis.downwards_arrow(0, point, ColorRGBA(0, 1, 0, 1), 'direct_move', '/map')
        self.lock.release()

    def handle_clicked_point(self, point):
        self.handle_move_goal(point.point)

    def handle_bpgt(self, data):
        self.lock.acquire()
        self.position, self.yaw = util.extract_pos_yaw(data)
        self.lock.release()

    def run(self):
        # wait for sensor data
        while self.target is None:
            rospy.sleep(0.1)

        rate_hz = 5
        rate = rospy.Rate(rate_hz)
        self.mover.set_rate(rate_hz)

        while not rospy.is_shutdown():
            rate.sleep()

            self.lock.acquire()

            # relative direction
            direction = [self.target.x - self.position.x, self.target.y - self.position.y]

            # stopping distance
            if np.linalg.norm(direction) < 0.1:
                self.mover.move(direction=np.array((0, 0)))
            else:
                direction = util.rotate_vector(direction, -1 * self.yaw)
                self.mover.move(direction=direction, sensor_data=self.sensor_data)

            self.lock.release()

            self.pub_marker.publish(self.target_marker)

if __name__ == '__main__':
    try:
        node = Direct()
        print "use rviz to set a path or publish to the move_goal topic"
        node.run()
    except rospy.ROSInterruptException:
        pass
