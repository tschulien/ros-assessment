import numpy as np
import util
import vis
import constants
import vis_rays

class SafeMove:

    # about 0.112
    diameter = np.sqrt(0.1 ** 2 + 0.05 ** 2)

    def __init__(self, pub_marker):
        self.pub_marker = pub_marker

    def safe_turn(self, direction, sensor_data):
        rear_angle = np.arctan(0.5)
        safety_bit = 0.08

        left_p0 = util.scan_pos(sensor_data, len(sensor_data.ranges) - 1)
        left_p1 = util.scan_pos(sensor_data, len(sensor_data.ranges) - 2)

        right_p0 = util.scan_pos(sensor_data, 0)
        right_p1 = util.scan_pos(sensor_data, 1)

        # calculate orthogonal vectors
        left_w = left_p0 - left_p1
        left_ortho = left_p0 - np.dot(left_p0, left_w) / np.dot(left_w, left_w) * left_w
        left_wall_dist = np.linalg.norm(left_ortho)

        right_w = right_p0 - right_p1
        right_ortho = right_p0 - np.dot(right_p0, right_w) / np.dot(right_w, right_w) * right_w
        right_wall_dist = np.linalg.norm(right_ortho)

        # calculate angles
        if abs(left_wall_dist) > self.diameter:
            left_rear2_angle = float('nan')
        else:
            left_rear2_angle = np.arccos(left_wall_dist / self.diameter)
        left_base_angle = np.pi / 2 - left_rear2_angle - rear_angle
        left_ortho_angle = np.arctan2(left_ortho[1], left_ortho[0])

        if abs(right_wall_dist) > self.diameter:
            right_rear2_angle = float('nan')
        else:
            right_rear2_angle = np.arccos(right_wall_dist / self.diameter)
        right_base_angle = np.pi / 2 - right_rear2_angle - rear_angle
        right_ortho_angle = np.arctan2(right_ortho[1], right_ortho[0])

        # calculate critical angles
        left_critical_angle = left_ortho_angle + (left_rear2_angle + rear_angle) - np.pi + safety_bit
        right_critical_angle = right_ortho_angle - (right_rear2_angle + rear_angle) + np.pi - safety_bit

        dir_angle = np.arctan2(direction[1], direction[0])

        changed = False
        if left_wall_dist < 0.2 and dir_angle < left_critical_angle and left_critical_angle < 0.2:
            self.pub_marker.publish(vis.arrow_from_to(0, left_p0, left_p1 - left_p0, constants.yellow, 'turn_safe', '/real_robot_pose'))
            self.pub_marker.publish(vis.arrow_from_to(1, (0, 0), left_ortho, constants.green, 'turn_safe', '/real_robot_pose'))
            self.pub_marker.publish(vis.arrow_angle(2, left_critical_angle, 1, constants.green, 'turn_safe', '/real_robot_pose'))

            dir_angle = left_critical_angle
            changed = True
        else:
            self.pub_marker.publish(vis.delete(0, 'turn_safe'))
            self.pub_marker.publish(vis.delete(1, 'turn_safe'))
            self.pub_marker.publish(vis.delete(2, 'turn_safe'))

        if right_wall_dist < 0.2 and dir_angle > right_critical_angle and right_critical_angle > -0.2:
            self.pub_marker.publish(vis.arrow_from_to(10, right_p0, right_p1 - right_p0, constants.orange, 'turn_safe', '/real_robot_pose'))
            self.pub_marker.publish(vis.arrow_angle(11, right_critical_angle, 1, constants.red, 'turn_safe', '/real_robot_pose'))
            self.pub_marker.publish(vis.arrow_from_to(12, (0, 0), right_ortho, constants.red, 'turn_safe', '/real_robot_pose'))
            dir_angle = right_critical_angle
            changed = True
        else:
            self.pub_marker.publish(vis.delete(10, 'turn_safe'))
            self.pub_marker.publish(vis.delete(11, 'turn_safe'))
            self.pub_marker.publish(vis.delete(12, 'turn_safe'))

        if changed:
            return 0.06 * np.array((np.cos(dir_angle), np.sin(dir_angle)))
        return direction

    def safe_direction(self, direction, sensor_data):
        direction = np.array(direction)
        # return self.safe_turn(direction, sensor_data)
        direction_len = np.linalg.norm(direction)
        if direction_len == 0:
            print "dir length is zero"
            return direction

        # TODO: tweak parameters
        dx = 0.07
        safe_r = 0.1
        desired = 0.5 * (dx / np.linalg.norm(direction) * direction + np.array((dx, 0)))

        left_shift = np.array((0, 0))
        right_shift = np.array((0, 0))
        last_dist = float('inf')
        is_right = True
        closest_left = safe_r
        closest_right = safe_r

        for i, dist in enumerate(sensor_data.ranges):
            p = util.scan_pos(sensor_data, i)
            # check if still connected
            if is_right:
                if dist > last_dist + 0.1:
                    # not connected anymore, now it's the other side
                    is_right = False
                last_dist = dist
            
            v = desired - p
            shift_dist = np.linalg.norm(v)
            if shift_dist < safe_r:
                if is_right:
                    if shift_dist < closest_right:
                        closest_right = shift_dist
                        right_shift = (safe_r - shift_dist) / shift_dist * v
                else:
                    if shift_dist < closest_left:
                        closest_left = shift_dist
                        left_shift = (safe_r - shift_dist) / shift_dist * v

        self.pub_marker.publish(vis.arrow_from_to(0, (0, 0), desired, constants.yellow, 'safe', '/real_robot_pose'))
        self.pub_marker.publish(vis.arrow_from_to(1, desired, left_shift, constants.red, 'safe', '/real_robot_pose'))
        self.pub_marker.publish(vis.arrow_from_to(2, desired, right_shift, constants.green, 'safe', '/real_robot_pose'))

        # no corrections have been done?
        if (left_shift == np.array((0, 0))).all() and (right_shift == np.array((0, 0))).all():
            # move at full speed
            desired = direction
        else:
            desired = desired + left_shift + right_shift

        return self.safe_turn(desired, sensor_data)
