#!/usr/bin/env python

# circles the object in front of the robot and
# takes some snapshots from time to time using the take_snapshot topic

import rospy
from std_msgs.msg import Bool
from nav_msgs.msg import Odometry
from sensor_msgs.msg import LaserScan
import threading
from move import Move
import util
import vis_rays
import numpy as np
import constants

class Paparazzi:

    photo_distance = 1.1
    photo_interval = 0.5
    wall_dist = 0.3

    def __init__(self):
        self.lock = threading.Lock()
        rospy.init_node("paprazzi")

        # set by the callbacks
        self.sensor_data = None
        self.position = None
        self.yaw = None

        # subscribers
        rospy.Subscriber("/base_pose_ground_truth", Odometry, self.handle_bpgt)
        rospy.Subscriber("/base_scan", LaserScan, self.handle_scan)

        # publishers
        self.mover = Move()
        self.pubTakePhoto = rospy.Publisher("/take_snapshot", Bool, queue_size=5)

        # the position where the robot left the object to take a photo
        self.exit_pos = None
        # the position where the robot wants to take a photo
        self.snapshot_pos = None

    def handle_bpgt(self, bpgt):
        self.lock.acquire()
        self.position, self.yaw = util.extract_pos_yaw_np(bpgt)
        self.lock.release()

    def handle_scan(self, scan):
        self.lock.acquire()
        self.sensor_data = scan
        self.lock.release()

    def follow_object(self):
        partition = vis_rays.partition_laser(self.sensor_data)

        # front sensor has highest priority
        if partition.front.distance < 0.5:
            return util.get_desired(
                angle=partition.front.angle,
                dist=partition.front.distance,
                dx=0.3,
                turn_right=True,
                safe_dist=0.15)
        elif partition.left.distance < Paparazzi.wall_dist:
            return util.get_desired(
                angle=partition.left.angle,
                dist=partition.left.distance,
                dx=0.3,
                turn_right=True,
                safe_dist=0.15)
        else:
            # no front and no left sensor, the robot lost the wall
            return [0.05, 0.1]

    def abs_to_rel(self, absolute):
        # TODO: replace with tf maybe?
        return util.rotate_vector(absolute - self.position, -1 * self.yaw)

    def rel_to_abs(self, relative):
        # TODO: replace with tf maybe?
        return util.rotate_vector(relative, self.yaw) + self.position

    def run(self):
        print "Wait for sensors"
        while self.position is None or self.sensor_data is None:
            rospy.sleep(0.01)

        rate = rospy.Rate(5)
        self.mover.set_rate(5)

        while not rospy.is_shutdown():
            rate.sleep()

            self.lock.acquire()

            # drive forward until there is an obstacle
            partition = vis_rays.partition_laser(self.sensor_data, front_size=0.2)
            self.mover.move([1, 0])

            self.lock.release()

            if partition.front.distance < 0.2:
                break

        print "Let's take some photos!"
        self.exit_pos = self.position

        state_follow = 0
        state_leave = 1
        state_return = 2
        state_take_photo = 3
        state = state_follow

        while not rospy.is_shutdown():
            rate.sleep()

            self.lock.acquire()

            turn_only = False
            colour = constants.black

            if state == state_follow:
                if np.hypot(self.exit_pos[0] - self.position[0], self.exit_pos[1] - self.position[1]) > Paparazzi.photo_interval:
                    # this is a good moment to take another photo
                    self.exit_pos = self.position
                    self.snapshot_pos = self.rel_to_abs([0, -Paparazzi.photo_distance])
                    state = state_leave
                else:
                    desired_direction = self.follow_object()
                    colour = constants.red

            if state == state_leave:
                desired_direction = self.abs_to_rel(self.snapshot_pos)
                colour = constants.orange

                partition = vis_rays.partition_laser(self.sensor_data, front_size=0.1)

                # move forward until the robot reached the snapshot position or an obstacle is blocking it's way
                if np.linalg.norm(desired_direction) < 0.1 or partition.front.distance < 0.3:
                    state = state_take_photo

            if state == state_take_photo:
                # turn to the right angle
                desired_direction = self.abs_to_rel(self.exit_pos)
                colour = constants.green

                if abs(desired_direction[1]) < 0.1 and desired_direction[0] > 0:
                    # TODO: is it really important to stop? maybe better practise?
                    self.mover.blocking_stop(angular_tolerance=0.01)
                    rospy.sleep(0.1)
                    self.pubTakePhoto.publish(Bool(True))
                    rospy.sleep(0.1)
                    state = state_return
                else:
                    turn_only = True

            if state == state_return:
                desired_direction = self.abs_to_rel(self.exit_pos)
                colour = constants.yellow

                if np.linalg.norm(desired_direction) < 0.2:
                    state = state_follow

            self.mover.move(desired_direction, sensor_data=self.sensor_data, turn_only=turn_only, arrow_colour=colour)

            self.lock.release()


if __name__ == '__main__':
    try:
        node = Paparazzi()
        node.run()
    except rospy.ROSInterruptException:
        pass
