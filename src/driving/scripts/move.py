# feed it a direction and it will move smoothly

import rospy
import numpy as np
from std_msgs.msg import ColorRGBA
from geometry_msgs.msg import Twist
from visualization_msgs.msg import Marker
import util
import vis
import constants
from safe_move import SafeMove

class Move:

    def __init__(self):
        # needs to be set manually
        # time between each move
        self.dt = 0

        # one half turn per second
        self.angular_accel = 1.5 * np.pi
        self.angular_max = 1.5 * np.pi
        self.angular_tol = 0.1 * np.pi
        self.last_angular = 0

        # acceleration
        self.linear_accel = 0.05
        # maximum velocity
        # self.linear_max = 0.05
        self.linear_max = 0.1
        # target reached tolerance
        # self.linear_tol = 0.1
        self.linear_tol = 0.05
        # velocity of the last move
        self.last_linear = 0

        self.pub = rospy.Publisher('/cmd_vel', Twist, queue_size=10)
        self.pub_marker = rospy.Publisher('/vis_move', Marker, queue_size=10)
        self.safe = SafeMove(self.pub_marker)

    def set_rate(self, rate_hz):
        self.dt = 1.0 / rate_hz

    def move(self, direction, arrow_colour=ColorRGBA(0.2, 0.2, 0.6, 1), sensor_data=None, turn_only=False):
        self.pub_marker.publish(vis.arrow_from_to(1, [0, 0], direction, constants.orange, 'desired_direction', '/real_robot_pose'))
        if not sensor_data is None:
            safe_dir = self.safe.safe_direction(direction, sensor_data)
            if not safe_dir is None:
                direction = safe_dir

        target_angle = np.arctan2(direction[1], direction[0])

        tw = Twist()
        tw.angular.z = util.lerp(
            current_vel=self.last_angular,
            signed_distance=target_angle,
            accel=self.angular_accel,
            dt=self.dt,
            vel_max=self.angular_max,
            tolerance=self.angular_tol,
        )

        tw.linear.x = util.lerp(
            current_vel=self.last_linear,
            signed_distance=(0 if turn_only else direction[0]),
            accel=self.linear_accel,
            dt=self.dt,
            vel_max=self.linear_max,
            tolerance=self.linear_tol,
        )

        self.pub.publish(tw)

        self.last_angular = tw.angular.z
        self.last_linear = tw.linear.x

        self.pub_marker.publish(vis.arrow_from_to(0, [0, 0], direction, arrow_colour, 'desired_direction', '/real_robot_pose'))

    def blocking_stop(self, linear_tolerance=0.05, angular_tolerance=0.05):
        # break until the velocities are below the given tolerances
        rate = rospy.Rate(1.0 / self.dt)
        while abs(self.last_linear) > linear_tolerance and abs(self.last_angular) > angular_tolerance:
            rate.sleep()
            self.move([0, 0])

    def rotate_around_edge(self, around_right):
        tw = Twist()
        # half the robot's width
        r = constants.robot_width / 2
        # note that velocities are limited by the world file
        tw.linear.x = 0.05
        sign = -1 if around_right else 1
        tw.angular.z = sign * tw.linear.x / r
        self.pub.publish(tw)