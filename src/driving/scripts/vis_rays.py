#!/usr/bin/env python

from collections import namedtuple
import rospy
from geometry_msgs.msg import Point, Quaternion
from sensor_msgs.msg import LaserScan
from visualization_msgs.msg import Marker
import tf
import numpy as np
import vis
import constants
import util

np.seterr(all='warn')

LaserPartition = namedtuple('LaserPartition', ['left', 'front', 'right'])
LaserPart = namedtuple('LaserPart', ['distance', 'angle'])

default_front_size = 0.05
sides_size = 0.1
laser_thresh = 0.15

def marker(marker_id, dist, angle, ns):
    arrow = Marker()
    arrow.id = marker_id
    arrow.type = Marker.ARROW
    arrow.action = Marker.ADD
    arrow.ns = ns

    arrow.header.frame_id = '/real_robot_pose'
    arrow.header.stamp = rospy.Time.now()

    q = tf.transformations.quaternion_from_euler(0, 0, angle)
    # convert to geometry_msgs's quaternion
    arrow.pose.orientation = Quaternion(q[0], q[1], q[2], q[3])
    arrow.pose.position = Point(0, 0, 0)

    arrow.scale.x = dist
    arrow.scale.y = 0.01
    arrow.scale.z = 0.01

    return arrow

def partition_laser(sensor_data, use_inf=True, front_size=default_front_size):
    # robot size is 0.1 x 0.1
    # only detect rays that would block the robot as front rays

    min_dist_front = float('inf')
    min_dist_left = float('inf')
    min_dist_right = float('inf')

    min_angle_front = None
    min_angle_left = None
    min_angle_right = None

    for i, dist in enumerate(sensor_data.ranges):
        angle = sensor_data.angle_min + i * sensor_data.angle_increment

        if abs(np.sin(angle)) * dist < front_size:
            # front sensor
            if dist < min_dist_front:
                min_dist_front = dist
                min_angle_front = angle
        if angle < 0 and np.cos(angle) * dist < sides_size:
            # right sensor
            if dist < min_dist_right:
                min_dist_right = dist
                min_angle_right = angle
        if angle > 0 and np.cos(angle) * dist < sides_size:
            # left sensor
            if dist < min_dist_left:
                min_dist_left = dist
                min_angle_left = angle

    if use_inf:
        # replace max range values with infinity
        if min_dist_front == sensor_data.range_max:
            min_dist_front = float('inf')
        if min_dist_left == sensor_data.range_max:
            min_dist_left = float('inf')
        if min_dist_right == sensor_data.range_max:
            min_dist_right = float('inf')

    return LaserPartition(
        left=LaserPart(min_dist_left, min_angle_left),
        front=LaserPart(min_dist_front, min_angle_front),
        right=LaserPart(min_dist_right, min_angle_right))

def recognise_obstacles(sensor_data, as_points=True, allow_inf=False):
    inf = 2 * sensor_data.range_max # maybe replace with float('inf')
    last_dist = inf

    min_dist = inf
    min_i = None

    obstacles = []
    current_obstacle = []

    for i, dist in enumerate(sensor_data.ranges):
        angle = sensor_data.angle_min + i * sensor_data.angle_increment

        if dist == sensor_data.range_max:
            dist = inf

        if abs(last_dist - dist) > laser_thresh:
            # different obstacle
            if current_obstacle:
                obstacles.append(current_obstacle)
            current_obstacle = []

        if dist < inf:
            if as_points:
                current_obstacle.append(util.angle_dist_pos(angle, dist))
            else:
                current_obstacle.append((angle, dist))
        elif allow_inf:
            # add single point obstacles for infinite rays
            if as_points:
                obstacles.append([util.angle_dist_pos(angle, sensor_data.range_max)])
            else:
                obstacles.append([(angle, sensor_data.range_max)])

        if dist < min_dist:
            min_dist = dist
            # index of this obstacle
            min_i = len(obstacles)

        last_dist = dist

    if current_obstacle:
        obstacles.append(current_obstacle)

    return obstacles, min_i

def callback(data):
    # draw_min_partition(data)
    # draw_partitions(data)
    draw_all(data)
    # obstacles, min_i = recognise_obstacles(data, allow_inf=True)
    # for m in draw_obstacles(obstacles):
    #     pub.publish(m)

    # draw_safe(data)

def draw_min_partition(data):
    ns = 'min_partition'
    partition = partition_laser(data)
    if partition.left.distance < 3:
        arrow = marker(0, partition.left.distance, partition.left.angle, ns)
        arrow.color = constants.red
    else:
        arrow = Marker()
        arrow.id = 0
        arrow.ns = 'min_partition'
        arrow.action = Marker.DELETE
    pub.publish(arrow)

    if partition.front.distance < 3:
        arrow = marker(1, partition.front.distance, partition.front.angle, ns)
        arrow.color = constants.blue
    else:
        arrow = Marker()
        arrow.id = 1
        arrow.action = Marker.DELETE
    pub.publish(arrow)

    if partition.right.distance < 3:
        arrow = marker(2, partition.right.distance, partition.right.angle, ns)
        arrow.color = constants.green
    else:
        arrow = vis.delete(2, ns)
    pub.publish(arrow)

def draw_partitions(data, front_size=default_front_size):
    partition = partition_laser(data)
    for i, dist in enumerate(data.ranges):
        angle = data.angle_min + i * data.angle_increment
        arrow = marker(i, dist, angle, 'partition')

        if abs(np.sin(angle)) * dist < front_size:
            # forward rays
            arrow.color = constants.green

        elif angle > 0 and np.cos(angle) * dist < sides_size:
            # left rays
            arrow.color = constants.blue
            if angle == partition.left.angle:
                arrow.color = constants.red

        elif angle < 0 and np.cos(angle) * dist < sides_size:
            # right rays
            arrow.color = constants.orange
            if angle == partition.right.angle:
                arrow.color = constants.gold
        else:
            arrow.action = Marker.DELETE

        pub.publish(arrow)

def draw_all(data):
    for i, dist in enumerate(data.ranges):
        angle = data.angle_min + i * data.angle_increment
        arrow = marker(i, dist, angle, 'all')

        if data.intensities[i] == 1.0:
            arrow.color = constants.gold
        else:
            arrow.color = constants.black

        pub.publish(arrow)

def draw_obstacles(obstacles):
    markers = []
    for i, obst in enumerate(obstacles):
        line = Marker()
        line.header.stamp = rospy.Time.now()
        line.header.frame_id = '/real_robot_pose'
        line.ns = 'obstacles'
        line.id = i
        line.action = Marker.ADD
        line.type = Marker.LINE_STRIP
        line.color = constants.blue
        line.scale.x = line.scale.y = line.scale.z = 0.02
        for pos in obst:
            line.points.append(Point(pos[0], pos[1], 0))

        markers.append(line)

    for i in range(len(obstacles), 10):
        markers.append(vis.delete(i, 'obstacles'))

    return markers

def draw_safe(scan):
    # get the scan as points
    points = [util.angle_dist_pos(util.scan_angle(scan, i), dist) for i, dist in enumerate(scan.ranges)]

    safe_r = 0.1

    spheres_left = Marker()
    spheres_right = Marker()
    spheres_left.header.stamp = rospy.Time.now()
    spheres_left.header.frame_id = '/real_robot_pose'
    spheres_right.header = spheres_left.header

    spheres_left.ns = 'safe_zones'
    spheres_right.ns = 'safe_zones'
    spheres_left.id = 0
    spheres_right.id = 1

    spheres_left.action = Marker.ADD
    spheres_right.action = Marker.ADD
    spheres_left.type = Marker.SPHERE_LIST
    spheres_right.type = Marker.SPHERE_LIST

    spheres_left.color = constants.orange
    spheres_right.color = constants.green
    spheres_left.color.a = 0.3
    spheres_right.color.a = 0.3

    spheres_left.scale.x = spheres_left.scale.y = spheres_left.scale.z = 2 * safe_r
    spheres_right.scale.x = spheres_right.scale.y = spheres_right.scale.z = 2 * safe_r
    spheres_left.pose.orientation = Quaternion(0, 0, 0, 1)
    spheres_right.pose.orientation = Quaternion(0, 0, 0, 1)

    left_points = []
    right_points = []

    last_dist = float('inf')
    is_right = True
    for p in points:
        if is_right:
            dist = np.linalg.norm(p)
            if dist > last_dist + 0.1:
                is_right = False
            last_dist = dist
        # if p[1] > 0:
        if not is_right:
            spheres_left.points.append(Point(p[0], p[1], 0))
            left_points.append(p)
        else:
            spheres_right.points.append(Point(p[0], p[1], 0))
            right_points.append(p)

    pub.publish(spheres_left)
    pub.publish(spheres_right)

    intersects = Marker()
    intersects.action = Marker.ADD
    intersects.id = 0
    intersects.ns = 'intersects'
    intersects.header.stamp = rospy.Time.now()
    intersects.header.frame_id = '/real_robot_pose'
    intersects.type = Marker.SPHERE_LIST
    intersects.color = constants.red
    intersects.scale.x = intersects.scale.y = intersects.scale.z = 0.05
    intersects.pose.orientation = Quaternion(0, 0, 0, 1)

    for pl in left_points:
        for pr in right_points:
            # check if the circles are intersecting
            dist = np.linalg.norm(pl - pr)

            if dist < safe_r:
                # center is contained in the other circle
                pass
            elif dist < 2 * safe_r:
                # circles intersect
                mid = 0.5 * (pl + pr)
                intersects.points.append(Point(mid[0], mid[1], 0))
            else:
                # no intersection
                pass

    pub.publish(intersects)

if __name__ == '__main__':
    rospy.init_node('vis_rays')
    pub = rospy.Publisher('vis_rays', Marker, queue_size=100)
    rospy.Subscriber('/base_scan', LaserScan, callback)

    rospy.spin()
