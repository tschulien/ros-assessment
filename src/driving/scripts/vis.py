import rospy
from geometry_msgs.msg import Quaternion, Point, Vector3
from visualization_msgs.msg import Marker
import tf
import numpy as np

def sphere(marker_id, position, radius, colour, namespace, frame_id):
    mr = Marker()
    mr.id = marker_id
    mr.ns = namespace
    mr.type = Marker.SPHERE
    mr.action = Marker.ADD
    mr.header.frame_id = frame_id
    mr.header.stamp = rospy.Time.now()
    mr.pose.position = Point(position[0], position[1], 0)
    mr.scale = Vector3(2 * radius, 2 * radius, 2 * radius)
    mr.color = colour
    return mr

def downwards_arrow(marker_id, position, colour, namespace, frame_id='/map'):
    arrow = Marker()
    arrow.id = marker_id
    arrow.type = Marker.ARROW
    arrow.action = Marker.ADD
    arrow.header.frame_id = frame_id
    arrow.header.stamp = rospy.Time.now()
    arrow.ns = namespace
    # make the arrow pointing downwards
    arrow.pose.orientation = Quaternion(0.5, 0.5, -0.5, 0.5)
    arrow.scale = Vector3(0.5, 0.05, 0.05)
    arrow.pose.position = Point(position.x, position.y, position.z + arrow.scale.x)
    arrow.color = colour
    return arrow

def arrow_from_to(marker_id, origin, direction, colour, namespace, frame_id, scale=0.02):
    arrow = Marker()
    arrow.id = marker_id
    arrow.type = Marker.ARROW
    arrow.action = Marker.ADD
    arrow.header.frame_id = frame_id
    arrow.header.stamp = rospy.Time.now()
    arrow.ns = namespace

    length = np.linalg.norm(direction)
    if length == 0:
        arrow.action = Marker.DELETE
        return arrow

    angle = np.arctan2(direction[1], direction[0])
    q = tf.transformations.quaternion_from_euler(0, 0, angle)
    # convert to msg quaternion
    arrow.pose.orientation = Quaternion(q[0], q[1], q[2], q[3])

    arrow.scale = Vector3(length, scale, scale)

    arrow.pose.position = Point(origin[0], origin[1], 0)
    arrow.color = colour
    return arrow

def arrow_angle(marker_id, angle, dist, colour, namespace, frame_id, origin=Point(0, 0, 0), scale=0.02):
    arrow = Marker()
    arrow.id = marker_id
    arrow.type = Marker.ARROW
    arrow.action = Marker.ADD
    arrow.ns = namespace

    arrow.header.frame_id = frame_id
    arrow.header.stamp = rospy.Time.now()

    q = tf.transformations.quaternion_from_euler(0, 0, angle)
    # convert to geometry_msgs's quaternion
    arrow.pose.orientation = Quaternion(q[0], q[1], q[2], q[3])
    arrow.pose.position = origin

    arrow.scale.x = dist
    arrow.scale.y = scale
    arrow.scale.z = scale

    arrow.color = colour
    return arrow

def delete(marker_id, namespace):
    marker = Marker()
    marker.ns = namespace
    marker.id = marker_id
    marker.action = Marker.DELETE
    return marker
