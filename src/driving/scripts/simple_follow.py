#!/usr/bin/env python

import sys
import rospy
from std_msgs.msg import ColorRGBA
from path_planning.srv import AStar
from nav_msgs.msg import Path, Odometry
from geometry_msgs.msg import Point, Quaternion, PoseStamped
from sensor_msgs.msg import LaserScan
from visualization_msgs.msg import Marker
import threading
import numpy as np
import util
from move import Move
import vis
import vis_rays
import constants


class FollowPathSimple:

    path_distance = 0.1
    wall_dist = 0.3
    # wall_dist_front = 0.35
    dx = 0.15
    safe_dist = 0.15
    blocked_dist = 0.2
    reach_thresh = 0.08

    def __init__(self, vis_only=False):
        self.lock = threading.Lock()
        rospy.init_node('follow_path')

        self.position = None
        self.yaw = None
        self.scan = None

        self.pub_vis = rospy.Publisher('vis_direct_move', Marker, queue_size=5)
        self.pub_path = rospy.Publisher('vis_path', Path, queue_size=5)
        rospy.Subscriber('/base_pose_ground_truth', Odometry, self.handle_bpgt)
        rospy.Subscriber('/base_scan', LaserScan, self.handle_scan)
        self.mover = Move()

        self.vis_path = Path()
        self.vis_path.header.frame_id = '/map'

        self.path = []
        self.isGoal = []
        self.vis_only = vis_only

        print "Waiting for salesman service"
        rospy.wait_for_service('salesman')

    def handle_bpgt(self, odom):
        self.lock.acquire()
        self.position, self.yaw = util.extract_pos_yaw_np(odom)
        self.lock.release()

    def handle_scan(self, scan):
        self.lock.acquire()
        self.scan = scan
        self.lock.release()

    def visualise_path(self):
        for p in self.path:
            ps = PoseStamped()
            ps.header.frame_id = '/map'
            ps.header.stamp = rospy.Time.now()
            ps.pose.orientation = Quaternion(0, 0, 0, 1)
            ps.pose.position = p
            self.vis_path.poses.append(ps)

        self.pub_path.publish(self.vis_path)

    def find_path_form_params(self, start_pos):
        # collect all targets
        targets = []
        goalIndex = 0
        while rospy.has_param('goal{}'.format(goalIndex)):
            result = rospy.get_param('goal{}'.format(goalIndex))
            point = Point(x=result[0], y=result[1], z=0)
            targets.append(point)
            goalIndex += 1

        try:
            find_path = rospy.ServiceProxy('salesman', AStar)
            print "Ask the salesman"
            resp = find_path(Point(start_pos[0], start_pos[1], 0), targets)
            if resp.success == 0:
                raise Exception("Path finding failed")

            self.path = resp.path
            self.isGoal = [p in targets for p in resp.path]
            self.visualise_path()

        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

    def follow_wall(self, turn_right=True):
        # turns to the right
        partition = vis_rays.partition_laser(self.scan)

        side_distance = partition.left.distance if turn_right else partition.right.distance
        side_angle = partition.left.angle if turn_right else partition.right.angle

        # front sensor has highest priority
        if partition.front.distance < self.wall_dist:
            return util.get_desired(
                angle=partition.front.angle,
                dist=partition.front.distance,
                dx=self.dx,
                turn_right=turn_right,
                safe_dist=self.safe_dist)
        elif side_distance < self.wall_dist:
            return util.get_desired(
                angle=side_angle,
                dist=side_distance,
                dx=self.dx,
                turn_right=turn_right,
                safe_dist=self.safe_dist)
        else:
            # no front and no left sensor, the robot lost the wall
            if turn_right:
                return [0.05, 0.1]
            else:
                return [0.05, -0.1]

    def is_blocked(self, point):
        # TODO: improve angle calculation (no matrix multiplication required)
        relative = point - self.position
        relative = util.rotate_vector(relative, -1 * self.yaw)
        target_angle = np.arctan2(relative[1], relative[0])
        target_dist = np.linalg.norm(relative)

        last_dist = float('inf')
        for i, dist in enumerate(self.scan.ranges):
            angle = util.scan_angle(self.scan, i)
            if angle > target_angle:
                # check this distance and the last distance
                if (dist < target_dist and dist < self.blocked_dist) or (last_dist < target_dist and last_dist < self.blocked_dist):
                    self.pub_vis.publish(vis.arrow_angle(0, angle, dist, constants.red, 'blocked', '/real_robot_pose'))
                    self.pub_vis.publish(vis.arrow_angle(1, angle - self.scan.angle_increment, last_dist, constants.blue, 'blocked', '/real_robot_pose'))
                    print target_dist, "dist"
                    print last_dist, dist
                    return True
                else:
                    return False

            last_dist = dist

        return False

    def line_segment_distance(self, start, end):
        line = end - start
        v = self.position - start

        dot_v_line = np.dot(v, line)
        dot_line_line = np.dot(line, line)
        if dot_v_line > 0 and dot_v_line < dot_line_line:
            # between the points
            ortho = v - (dot_v_line / dot_line_line) * line
            return np.linalg.norm(ortho)
        else:
            # not between
            w = self.position - end
            return min(np.linalg.norm(v), np.linalg.norm(w))


    def find_closest_path_point(self, last_target_index):
        # find the closest point on the path
        # the point should come after the already reached targets
        min_distance = float('inf')
        min_index = None

        # check every point after the last reached points
        last_point = np.array((self.path[last_target_index].x, self.path[last_target_index].y))
        for i, p in enumerate(self.path[(last_target_index + 1):]):
            npp = np.array((p.x, p.y))
            dist = self.line_segment_distance(last_point, npp)

            if dist < min_distance:
                min_distance = dist
                min_index = i

            # Is this a goal? It must not be skipped
            if self.isGoal[last_target_index + 1 + i]:
                break

            last_point = npp

        return min_distance, min_index + last_target_index + 1

    def run(self):
        print "Waiting for sensor data"
        while self.position is None:
            rospy.sleep(0.01)

        self.find_path_form_params(self.position)

        # do not follow the path (only visualise)
        if self.vis_only:
            return

        current_index = 0
        target = np.array((self.path[0].x, self.path[0].y))

        rate = rospy.Rate(5)
        self.mover.set_rate(5)

        target_colour = ColorRGBA(0, 1, 0, 0.3)

        # follow the path
        state_path = 0
        # follow the wall and get away from the path
        state_wall_leave = 1
        # follow the wall and get back to the path again
        state_wall_return = 2

        state = state_path

        path_leave_point = None

        turn_right = False

        while not rospy.is_shutdown():
            rate.sleep()

            self.lock.acquire()

            self.pub_vis.publish(vis.sphere(10, target, self.reach_thresh, target_colour, 'follow', '/map'))

            if state == state_path:
                # close enough to the target?
                direction = target - self.position
                direction = util.rotate_vector(direction, -1 * self.yaw)
                sensor_data = self.scan

                if np.linalg.norm(direction) < self.reach_thresh:
                    if current_index + 1 >= len(self.path):
                        # reached the final goal
                        break

                    current_index += 1
                    target = np.array((self.path[current_index].x, self.path[current_index].y))

                # check if blocking
                if self.is_blocked(target):
                    path_leave_point = self.position
                    state = state_wall_leave
                    print "leave"

            elif state == state_wall_leave:
                direction = self.follow_wall(turn_right)
                # far away enough from the path?
                distance, index = self.find_closest_path_point(current_index - 1)
                if distance > self.path_distance:
                    state = state_wall_return
                    print "return"

            elif state == state_wall_return:
                direction = self.follow_wall(turn_right)
                distance, index = self.find_closest_path_point(current_index - 1)
                # back again on the path
                if distance < self.path_distance:
                    current_index = index
                    target = np.array((self.path[current_index].x, self.path[current_index].y))
                    state = state_path
                    print "path"
            else:
                raise NotImplementedError("Unknown state")

            self.lock.release()

            # drive towards the target
            self.mover.move(direction, sensor_data=sensor_data)

        print "Reached the last goal"
        self.mover.blocking_stop()

if __name__ == '__main__':
    vis_only = False
    if len(sys.argv) > 1:
        if sys.argv[1] == 'v':
            vis_only = True
    try:
        node = FollowPathSimple(vis_only)
        node.run()
    except rospy.ROSInterruptException:
        pass
