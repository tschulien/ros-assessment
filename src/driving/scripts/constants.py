from std_msgs.msg import ColorRGBA

red = ColorRGBA(1, 0, 0, 1)
green = ColorRGBA(0, 1, 0, 1)
blue = ColorRGBA(0, 0, 1, 1)
orange = ColorRGBA(0.85, 0.6, 0.1, 1)
yellow = ColorRGBA(1, 1, 0, 1)
black = ColorRGBA(0, 0, 0, 1)
purple = ColorRGBA(0.5, 0.3, 0.8, 1)
gold = ColorRGBA(0.6, 0.7, 0, 1)

robot_width = 0.1
