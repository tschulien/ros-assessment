#!/usr/bin/env python

# TODO: a lot of these functions are not used anymore, refactor!

import numpy as np
import tf

def get_desired(angle, dist, dx, turn_right, safe_dist):
    # find safe distance point/direction
    rot_matrix = np.array([[ np.cos(angle), np.sin(angle) ],
                            [ -np.sin(angle), np.cos(angle) ]])
    direction = np.matmul([1, 0], rot_matrix)
    # maybe not needed
    direction = direction / np.linalg.norm(direction)

    sign = 1 if turn_right else -1
    dx_direction = sign * dx * np.array([direction[1], -direction[0]])

    desired_direction = (dist - safe_dist) * direction

    desired_direction = desired_direction + dx_direction
    return desired_direction

def scan_angle(sensor_data, index):
    return sensor_data.angle_min + index * sensor_data.angle_increment

def scan_pos(sensor_data, index):
    return angle_dist_pos(scan_angle(sensor_data, index), sensor_data.ranges[index])

def side_of_vector(base, test):
    # returns -1 if test is left of base
    # returns +1 if test is right of base
    return np.sign(test[0] * base[1] - test[1] * base[0])

def extract_pos_yaw(odom):
    position = odom.pose.pose.position
    # convert to tf's quaternion
    q = (
        odom.pose.pose.orientation.x,
        odom.pose.pose.orientation.y,
        odom.pose.pose.orientation.z,
        odom.pose.pose.orientation.w
    )
    yaw = tf.transformations.euler_from_quaternion(q)[2]

    return position, yaw

def extract_pos_yaw_np(odom):
    pos, yaw = extract_pos_yaw(odom)
    return np.array((pos.x, pos.y)), yaw

def cos_vector(v1, v2):
    return np.arccos(np.dot(v1, v2) / (np.linalg.norm(v1) * np.linalg.norm(v2)))

def get_normal3(p1, main, p3):
    p_1 = p1 - main
    p_3 = p3 - main
    o_r = np.array([main[1], -main[0]])
    u_o = np.array([-p_1[1], p_1[0]])
    v_n = p_3 / np.linalg.norm(p_3)
    u_n = p_1 / np.linalg.norm(p_1)
    s = np.sign(np.dot(p_3, o_r) * np.dot(u_o, p_3))
    w = u_n + v_n
    return s / np.linalg.norm(w) * w

def get_normal2(main, aux):
    v = aux - main
    v_r = np.array([v[1], -v[0]])

    s = -np.sign(np.dot(main, v_r))
    return s / np.linalg.norm(v_r) * v_r

def angle_dist_pos(angle, dist):
    return np.array([
        np.cos(angle) * dist,
        np.sin(angle) * dist])

def wall_safe(b1, b2, goal, d):
    # assuming points that are relative to the robot's position
    boundary = b2 - b1
    w = goal - b1
    ortho = (np.dot(w, boundary) / np.dot(boundary, boundary)) * boundary - w

    h = np.sign(np.dot(b1, ortho))

    if h > 0 and np.linalg.norm(ortho) >= d:
        # you are safe, no worries!
        return goal

    return goal + ortho - h * d / np.linalg.norm(ortho) * ortho

def wall_safe2(goal, neighbour, d):
    if neighbour is None:
        return (1 - d / np.linalg.norm(goal)) * goal

    v = neighbour - goal
    w = np.array((v[1], -v[0]))
    b = np.sign(np.dot(goal, w)) * d
    u = (b / np.linalg.norm(w)) * w
    return goal - u

def rotate(a):
    return np.array([[np.cos(a),  np.sin(a)],
                     [-np.sin(a), np.cos(a)]])

def rotate_vector(vector, angle):
    # TODO: probably done wrong, should be matrix times vector
    return np.matmul(vector, rotate(angle))

def lerp(current_vel, signed_distance, accel, dt, vel_max, tolerance):
    # if close enough and slow enough we can finally stand still
    if abs(current_vel) < accel * dt and abs(signed_distance) < tolerance:
        return signed_distance

    if abs(signed_distance) - tolerance < 0.5 * (current_vel ** 2) / accel:
        # decelerate
        return current_vel - np.sign(current_vel) * accel * dt

    if abs(current_vel) < vel_max:
        # accelerate
        return current_vel + np.sign(signed_distance) * accel * dt
    else:
        # decelerate
        return current_vel - np.sign(current_vel) * accel * dt
