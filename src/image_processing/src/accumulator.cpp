#include <ros/ros.h>
#include <std_msgs/Bool.h>
#include <std_msgs/ColorRGBA.h>
#include <sensor_msgs/PointCloud2.h>
#include <visualization_msgs/Marker.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/PointStamped.h>

#include <tf/transform_listener.h>
#include <tf/tf.h>

#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_ros/transforms.h>
// #include <pcl_ros/io/pcd_io.h>

#include <math.h>
#include <algorithm>
#include <string>

bool isRed(int r, int g, int b)
{
    return (r > 200 && g < 50 && b < 50);
}

bool hasNanOrInf(const pcl::PointXYZRGB &point)
{
    return isinf(point.x) || isinf(point.y) || isinf(point.z) || isnan(point.x) || isnan(point.y) || isnan(point.z);
}

class Accumulator
{
  private:

    ros::Subscriber subCloud, subTakeSnapshot;
    ros::Publisher pubVis;

    std::vector<geometry_msgs::Point> bigVisCloud;

    bool cloudIsSet = false;
    sensor_msgs::PointCloud2 lastCloud;

    tf::TransformListener *globalListener;

    int captureIndex = 0;

    void handleCloud(const sensor_msgs::PointCloud2& cloud)
    {
        cloudIsSet = true;
        lastCloud = cloud;
    }

    void handleTakeSnapshot(const std_msgs::Bool &dummy)
    {
        // the robot should now take a snapshot
        ROS_INFO("Click!");
        processCloud(*globalListener);
    }

    // void saveToFile(pcl::PointCloud<pcl::PointXYZRGB> &cloud)
    // {
    //     // ROS_INFO("size before: %lu", cloud.size());

    //     // // remove NaNs
    //     // for (int i = 0; i < cloud.size(); ++i)
    //     // {
    //     //     if (hasNanOrInf(cloud[i]))
    //     //     {
    //     //         cloud.erase(cloud.begin() + i);
    //     //         --i;
    //     //     }
    //     // }

    //     // ROS_INFO("size after: %lu", cloud.size());

    //     std::string filename = "pcl_capture/capture" + std::to_string(captureIndex) + ".pcd";
    //     int result = pcl::io::savePCDFile<pcl::PointXYZRGB>(filename, cloud, false);
    //     ++captureIndex;

    //     ROS_INFO("saved %d, result: %d", captureIndex, result);
    // }

    void visualise(pcl::PointCloud<pcl::PointXYZRGB> &cloud)
    {
        for (auto const& q: cloud)
        {
            if (hasNanOrInf(q))
                continue;

            // only display red points
            if (!isRed(q.r, q.g, q.b))
                continue;

            geometry_msgs::Point p;
            p.x = q.x;
            p.y = q.y;
            p.z = q.z;

            bigVisCloud.push_back(p);
        }
    }

    void snapshot(tf::TransformListener &listener)
    {
        // convert to PCL datatype
        pcl::PointCloud<pcl::PointXYZRGB> conv;
        pcl::fromROSMsg(lastCloud, conv);

        // transform to /base_link frame
        pcl::PointCloud<pcl::PointXYZRGB> baseLinkCloud, mapCloud;
        pcl_ros::transformPointCloud<pcl::PointXYZRGB>("/base_link", conv, baseLinkCloud, listener);

        // transform to /map frame
        tf::StampedTransform strans;
        try
        {
            listener.lookupTransform("/map", "/real_robot_pose", ros::Time(0), strans);
        }
        catch (tf2::ExtrapolationException ex)
        {
            ROS_WARN("Extrapolation error");
            return;
        }
        pcl_ros::transformPointCloud<pcl::PointXYZRGB>(baseLinkCloud, mapCloud, strans);

        // saveToFile(mapCloud);
        visualise(mapCloud);
    }

    void publishBigCloud()
    {
        visualization_msgs::Marker cloud;
        cloud.action = visualization_msgs::Marker::ADD;
        cloud.type = visualization_msgs::Marker::SPHERE_LIST;
        cloud.id = 0;
        cloud.ns = "accumulated";
        cloud.header.stamp = ros::Time::now();
        cloud.header.frame_id = "/map";

        cloud.scale.x = 0.05;
        cloud.scale.y = 0.05;
        cloud.scale.z = 0.05;

        cloud.points = bigVisCloud;
        cloud.color.r = 1.0;
        cloud.color.g = 0.0;
        cloud.color.b = 0.0;
        cloud.color.a = 1.0;

        pubVis.publish(cloud);
    }

    void processCloud(tf::TransformListener& listener)
    {
        if (cloudIsSet)
        {
            snapshot(listener);
            publishBigCloud();
        }
        else
        {
            ROS_INFO("no cloud yet");
        }
    }

  public:

    void run(int argc, char **argv)
    {
        ros::init(argc, argv, "accumulator");
        ros::NodeHandle n;

        subCloud = n.subscribe("/rgb_points", 1000, &Accumulator::handleCloud, this);
        subTakeSnapshot = n.subscribe("take_snapshot", 100, &Accumulator::handleTakeSnapshot, this);
        pubVis = n.advertise<visualization_msgs::Marker>("vis_accu", 10);

        ros::Rate rate(1);

        globalListener = new tf::TransformListener();

        ROS_INFO("Ready");

        ros::spin();
    }
};

int main(int argc, char **argv)
{
    Accumulator().run(argc, argv);
    return 0;
}
